# -*- coding: utf-8 -*-
#from probability import ProbDist
from collections import Counter
import sys
import pickle

def main(args):
    (file_name, c_gram, output) = args
    tokens_dist = Counter()
    with open(file_name) as f:
        for line in f:
            line = line.strip()
            line_tokens = split_line(line.split(), int(c_gram))
            for t in line_tokens:
                tokens_dist[t] += 1

    N = sum(tokens_dist.itervalues())
    print tokens_dist
    print N
    pickle.dump(tokens_dist, open(output, "wb"))
    return

def split_line(tokens, c_gram):
    chars = []
    for i in range(1, c_gram + 1):
        for token in tokens:
            for j in range(0, len(token), i):
                chars.append(token[j:j+i])
    return chars

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage: load_mono_data.py <input> <c-grams> <pickle-file>'
        sys.exit(1)
    else:
        main(sys.argv[1:])



