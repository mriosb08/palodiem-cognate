from collections import Counter

class FreqDist:
    def __ini__(self, samples=None):
        self.count = Counter(samples)

    def N(self):
        return sum(self.count.itervalues())

    def freq(self, sample):
        if self.N() == 0:
            return 0.0
        else:
            return float(self.count[sample]) / self.N()

    def B(self):
        return len(self.count)
