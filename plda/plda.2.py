import numpy as np
import pymc as pm

K = 2 # number of topics
V = 4 # number of words
D = 3 # number of documents

#TODO words id???
#num of words is he same as docs!!
#change V and D
doc_src = np.array([[1, 1, 1, 1], [1, 1, 1, 1], [0, 0, 0, 0]])
doc_trg = np.array([[1, 0, 1, 0], [1, 1, 1, 0], [1, 1, 0, 0]])
data = np.array([[1, 0, 1, 0], [1, 1, 1, 0], [1, 1, 0, 0]])

alpha = np.ones(K)
beta_1 = np.ones(V)
beta_2 = np.ones(V)

theta = pm.Container([pm.CompletedDirichlet("theta_%s" % i, pm.Dirichlet("ptheta_%s" % i, theta=alpha)) for i in range(D)])
phi1 = pm.Container([pm.CompletedDirichlet("phi_src_%s" % k, pm.Dirichlet("pphi_src_%s" % k, theta=beta_1)) for k in range(K)])
phi2 = pm.Container([pm.CompletedDirichlet("phi_trg_%s" % k, pm.Dirichlet("pphi_trg_%s" % k, theta=beta_2)) for k in range(K)])
Wd = [len(doc) for doc in doc_src]

z = pm.Container([pm.Categorical('z_%i' % d, 
                     p = theta[d], 
                     size=Wd[d],
                     value=np.random.randint(K, size=Wd[d]))
                  for d in range(D)])

w_src = pm.Container([pm.Categorical("w_src_%i_%i" % (d,i),
                    p = pm.Lambda('phi_z_%i_%i' % (d,i), 
                    lambda z=z[d][i], phi1=phi1: phi1[z]),
                    value=doc_src[d][i], 
                    observed=True) for d in range(D) for i in range(Wd[d])])



w_trg = pm.Container([pm.Categorical("w_trg_%i_%i" % (d,i),
                    p = pm.Lambda('phi_z_%i_%i' % (d,i), 
                    lambda z=z[d][i], phi2=phi2: phi2[z]),
                    value=doc_trg[d][i], 
                    observed=True) for d in range(D) for i in range(Wd[d])])

model = pm.Model([theta, phi1, phi2, z, w_src, w_trg])
mcmc = pm.MCMC(model)
mcmc.sample(100)
r = mcmc.trace('z_0')[:]
print r
print len(r)
