#include <boost/algorithm/string.hpp>

#include <vector>
#include <map>
#include "EditDistanceFeature2.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/TargetPhrase.h"
#include "moses/Phrase.h"
#include "moses/Word.h"
#include "util/exception.hh"
#include <stdlib.h> 

using namespace std;

namespace Moses
{
EditDistanceFeature2::EditDistanceFeature2(const std::string &line)
  :StatelessFeatureFunction(0, line) //there is a version only for dense features for spase use':StatelessFeatureFunction(0, line)'
{
  ReadParameters();
}

void EditDistanceFeature2::Load()
{
    ifstream inFile(m_simFile.c_str());
    UTIL_THROW_IF2(!inFile, "could not open file " << m_simFile);
    std::string line;
    while (getline(inFile, line)) 
    {
        //std::set<std::string> terms;
        vector<std::string> tokens;
        boost::split(tokens, line, boost::is_any_of("\t"));//format of file: word1'\t'word2'\t'score'\n'
        boost::algorithm::to_lower(tokens[0]);
        boost::algorithm::to_lower(tokens[1]);
        float score = atof(tokens[2].c_str());
        std::string key = tokens[0] + "|||" + tokens[1];
        simSpace[key] = score; //stores in hash keys words value score
    }
    
    inFile.close();
}

//vector<std:string> tokens EditDistanceFeature2::SplitToken(const std::string &token)
//{
//    vector<std::string> tokens;

//    return tokens;
//}

void EditDistanceFeature2::EvaluateInIsolation(const Phrase &source
                                   , const TargetPhrase &targetPhrase
                                   , ScoreComponentCollection &scoreBreakdown
                                   , ScoreComponentCollection &estimatedFutureScore) const
{
    //compares words from the source and target during decoding
    // returns the score of a pair of words that is believed to be a cognate from the cache
	for(size_t s_pos = 0; s_pos < source.GetSize(); s_pos++)
	{
		for(size_t t_pos = 0; t_pos < targetPhrase.GetSize(); t_pos++)
		{
			std::string source_word = source.GetWord(s_pos).ToString();
            std::string target_word = targetPhrase.GetWord(t_pos).ToString();
            std::string key = source_word + "|||" + target_word;
            if(simSpace.find(key) != simSpace.end()){ //if words from the source and target are in cache store score as a sparse feature
                float value = simSpace.at(key);
                scoreBreakdown.PlusEquals(this, key, value); //sparse feature related specifically to the pair or words
            }
		}
	}

}

void EditDistanceFeature2::EvaluateWithSourceContext(const InputType &input
                                   , const InputPath &inputPath
                                   , const TargetPhrase &targetPhrase
                                   , const StackVec *stackVec
                                   , ScoreComponentCollection &scoreBreakdown
                                   , ScoreComponentCollection *estimatedFutureScore) const
{
/*	if (targetPhrase.GetNumNonTerminals()) {
		  vector<float> newScores(m_numScoreComponents);
		  newScores[0] = - std::numeric_limits<float>::infinity();
		  scoreBreakdown.PlusEquals(this, newScores);
	}*/

}

void EditDistanceFeature2::EvaluateWhenApplied(const ChartHypothesis &hypo,
        ScoreComponentCollection* accumulator) const
{}

void EditDistanceFeature2::EvaluateWhenApplied(const Hypothesis& hypo,
                                   ScoreComponentCollection* accumulator) const
{}


void EditDistanceFeature2::SetParameter(const std::string& key, const std::string& value)
{
  //load cache of cognate words from ini file
  if (key == "cognate-file") {
    // set value here
    m_simFile = value;

  } else {
	  StatelessFeatureFunction::SetParameter(key, value);
  }
}

}

