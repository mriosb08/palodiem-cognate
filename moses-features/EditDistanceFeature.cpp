#include <vector>
#include "EditDistanceFeature.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/TargetPhrase.h"
#include "moses/Phrase.h"
#include "moses/Word.h"

using namespace std;

namespace Moses
{
EditDistanceFeature::EditDistanceFeature(const std::string &line)
  :StatelessFeatureFunction(1, line) //there is a version only for dense features for spase use':StatelessFeatureFunction(0, line)'
{
  ReadParameters();
}

void EditDistanceFeature::EvaluateInIsolation(const Phrase &source
                                   , const TargetPhrase &targetPhrase
                                   , ScoreComponentCollection &scoreBreakdown
                                   , ScoreComponentCollection &estimatedFutureScore) const
{
    //compares words from source and target to score edit distance during decoding
	float edit_distance = 0.0;
	unsigned int k = 0;
	for(size_t s_pos = 0; s_pos < source.GetSize(); s_pos++)
	{
		for(size_t t_pos = 0; t_pos < targetPhrase.GetSize(); t_pos++)
		{
			const size_t len1 = source.GetWord(s_pos).ToString().size() , len2 = targetPhrase.GetWord(t_pos).ToString().size();
			vector<unsigned int> col(len2+1), prevCol(len2+1);

			for(unsigned int i = 0; i < prevCol.size(); i++)
				prevCol[i] = i;
			for(unsigned int i = 0; i < len1; i++)
			{
				col[0] = i+1;
				for (unsigned int j = 0; j < len2; j++)
					col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1)
										, prevCol[j] + (source.GetWord(s_pos).ToString()[i]==targetPhrase.GetWord(t_pos).ToString()[j] ? 0 : 1) );
				col.swap(prevCol);
			}
			edit_distance += prevCol[len2]/ (float) len1; //average edit distance between source and target words
			k++;
		}
	}

	edit_distance = edit_distance / (float) k;
	scoreBreakdown.PlusEquals(this, edit_distance);
}

void EditDistanceFeature::EvaluateWithSourceContext(const InputType &input
                                   , const InputPath &inputPath
                                   , const TargetPhrase &targetPhrase
                                   , const StackVec *stackVec
                                   , ScoreComponentCollection &scoreBreakdown
                                   , ScoreComponentCollection *estimatedFutureScore) const
{
/*	if (targetPhrase.GetNumNonTerminals()) {
		  vector<float> newScores(m_numScoreComponents);
		  newScores[0] = - std::numeric_limits<float>::infinity();
		  scoreBreakdown.PlusEquals(this, newScores);
	}*/

}

void EditDistanceFeature::EvaluateWhenApplied(const Hypothesis& hypo,
                                   ScoreComponentCollection* accumulator) const
{}

void EditDistanceFeature::EvaluateWhenApplied(const ChartHypothesis &hypo,
                                        ScoreComponentCollection* accumulator) const
{}

/*void EditDistanceFeature::SetParameter(const std::string& key, const std::string& value)
{
  if (key == "arg") {
    // set value here
	// m_all = Scan<bool>(value);

  } else {
	  StatelessFeatureFunction::SetParameter(key, value);
  }
}*/

}

