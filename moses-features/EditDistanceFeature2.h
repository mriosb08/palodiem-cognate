#pragma once

#include <string>
#include <vector>
#include <map>
#include "StatelessFeatureFunction.h"
#include "moses/Word.h"

namespace Moses
{

class EditDistanceFeature2 : public StatelessFeatureFunction
{
private:
    std::string m_simFile;
    std::map <std::string, float> simSpace;        

public:
    EditDistanceFeature2(const std::string &line);

    bool IsUseable(const FactorMask &mask) const {
        return true;
    }

    void EvaluateInIsolation(const Phrase &source
                , const TargetPhrase &targetPhrase
                , ScoreComponentCollection &scoreBreakdown
                , ScoreComponentCollection &estimatedFutureScore) const;
    void EvaluateWithSourceContext(const InputType &input
                , const InputPath &inputPath
                , const TargetPhrase &targetPhrase
                , const StackVec *stackVec
                , ScoreComponentCollection &scoreBreakdown
                , ScoreComponentCollection *estimatedFutureScore = NULL) const;
    void EvaluateWhenApplied(const Hypothesis& hypo,
                ScoreComponentCollection* accumulator) const;
    void EvaluateWhenApplied(const ChartHypothesis &hypo,
                ScoreComponentCollection* accumulator) const;
    void Load();

    //void SplitToken(const std::string &token);
    void SetParameter(const std::string& key, const std::string& value);

};

}

