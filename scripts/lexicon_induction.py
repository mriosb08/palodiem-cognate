import sys
import distance
from collections import defaultdict

def main(argv):
    """
    Exhaustive search of wikipedia for cognates (slow)
    It compares each word of source with target to find the one with the minumum distance
    """
    (source_file, target_file, threshold) = argv
    with open(source_file) as sf:
        for line_s in sf:
            line_s = line_s.strip()
            (source_freq, source) = line_s.split('\t')
            tmp_scores = dict()
            with open(target_file) as tf:
                for line_t in tf:
                    line_t = line_t.strip()
                    (target_freq, target) = line_t.split('\t')
                    if int(target_freq) >= int(threshold) and int(source_freq) >= int(threshold):
                        tmp_scores[target] = distance.levenshtein(source.lower(), target.lower(), normalized=True)
                #pick the min score
                if len(tmp_scores) != 0:
                    (target_cognate, score) = min(tmp_scores.iteritems(), key=lambda tmp_scores: tmp_scores[1])  
                    print source,'\t', target_cognate, '\t', score
    return


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage: python lexicon_induction.py <source-cwb> <target-cwb> <freq-threshold>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
