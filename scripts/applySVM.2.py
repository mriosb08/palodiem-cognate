import sys
import random
from collections import defaultdict
import operator
def main(args):
    (glue_file, n_best, src_test) = args
    n_best = int(n_best)
    out_dict = defaultdict(list)
    with open(glue_file) as gf:
        for line in gf:
            line = line.strip()
            line = line.replace('#', '')
            (tokens, predict) = line.split('\t')
            try:
                (src, trg, rank) = tokens.split(',')
                out_dict[src.lower()].append((trg, predict))
            except:
                continue
    with open(src_test) as st:
        for src in st:
            src = src.strip()
            if src.lower() in out_dict:
                out_list = out_dict[src.lower()]
                sorted_list = sorted(out_list, key=operator.itemgetter(1))
                for trg, score in sorted_list[:n_best]:
                    print '%s\t%s\t%s'%(src, trg, score)

    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python applySVM.py <lex-pred-file> <n-best> <test-sample>'
        print 'note: paste the prediction file with the lex file'
        print 'eg: cut -f 27 -d ' ' wiki.vocab.test.1.features | paste - wiki.vocab.test.1.predic > wiki.vocab.test.1.features.predic'
        sys.exit(1)
    else:
        main(sys.argv[1:])
