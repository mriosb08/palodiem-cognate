import sys
import random
from collections import defaultdict
import operator
def main(args):
    (glue_file, n_best, n_sample) = args
    n_best = int(n_best)
    n_sample = int(n_sample)
    out_dict = defaultdict(list)
    with open(glue_file) as gf:
        for line in gf:
            line = line.strip()
            line = line.replace('#', '')
            (tokens, predict) = line.split('\t')
            try:
                (src, trg, rank) = tokens.split(',')
                out_dict[src].append((trg, predict))
            except:
                continue
        if n_sample != 0:
            keys = random.sample(out_dict, n_sample)
            for src in keys:
                out_list = out_dict[src]
                sorted_list = sorted(out_list, key=operator.itemgetter(1))
                for trg, score in sorted_list[:n_best]:
                    print '%s\t%s\t%s'%(src, trg, score)
        else:
            for src,out_list in out_dict.iteritems():
                sorted_list = sorted(out_list, key=float(operator.itemgetter(1)))
                for trg, score in sorted_list[:n_best]:
                    print '%s\t%s'%(src, trg)

    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python applySVM.py <lex-pred-file> <n-best> <n-sample>'
        print 'note: paste the prediction file with the lex file'
        print 'eg: cut -f 27 -d ' ' wiki.vocab.test.1.features | paste - wiki.vocab.test.1.predic > wiki.vocab.test.1.features.predic'
        sys.exit(1)
    else:
        main(sys.argv[1:])
