# -*- coding: utf-8 -*-
import sys
from collections import defaultdict

def main(args):
    (dict_file, n, c) = args
    n = int(n)
    c = int(c)
    pb = defaultdict(list)
    with open(dict_file) as df:
        for line in df:
            line = line.strip()
            (src, trg, score) = line.split(' ')
            pb[src].append((trg, float(score)))

    for src, trgs in pb.iteritems():
        trgs = sorted(trgs, key=lambda tup: tup[1])
        for (trg, score) in trgs[:n]:
            print '%s %s'%(src, trg)
    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python max_pb_dict.py <pb-dict> <n> <c>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
