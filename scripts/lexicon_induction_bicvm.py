# -*- coding: utf-8 -*-
import multiprocessing
import sys
import logging
from more_itertools import chunked
#import distance as distl
from collections import defaultdict
import re
#import tempfile
import subprocess
#from gensim import corpora, models, similarities
from scipy.spatial import distance
import distance as distlev
import numpy as np
import pickle
import operator
from space import Space
from utils import apply_tm

#model_src = None #source binary vectors from bicvm
#model_trg = None #target binary vectors from bicvm
target_sp  = None
theta1 = 0.2
theta2 = 0.8
flag_tm = False

def main(argv):
    """
    searchs for cogantes in wikipedia by maximazing similar spelling and similar context
    spelling is 1 - edit_distance(a,b)
    context are vectors from word embeddings
    """
    global target_sp
    #global w

    source_file = argv[0]
    target_file = argv[1]
    theta1 = argv[2]
    theta2 = argv[3]
    num_jobs = int(argv[4])
    output = argv[5]
    vec_src = argv[6]
    vec_trg = argv[7]
    tm_file = argv[8]
    n_best = int(argv[9])
    #chunks = list(chunked(load_source(source_file), num_jobs))
    source_words = [line.strip() for line in open(source_file)]
    target_words = set([line.strip() for line in open(target_file)])
    source_chunks = chunks(source_words, num_jobs)
    jobs = []
    target_list = defaultdict(list)
    if tm_file != 'None':
        flag_tm = True
    
    #model_src = models.word2vec.Word2Vec.load_word2vec_format(vec_src, binary=True)
    #model_trg = models.word2vec.Word2Vec.load_word2vec_format(vec_trg, binary=True)
    #model_src = pickle.load(open(vec_src, "rb"))
    #model_trg = pickle.load(open(vec_trg, "rb"))
    #w = float(w_file)
    print 'loading target'
    target_sp = Space.build(vec_trg, target_words)
    print 'load target'
    print 'trg words: ', len(target_words)
    print 'src words: ', len(source_words)
    for i in range(num_jobs):
        p = multiprocessing.Process(target=worker, args=(i, set(source_chunks[i]), target_words, output, vec_src, vec_trg, tm_file, n_best))
        p.start()
        jobs.append(p)
    for j in jobs:
        j.join()
    cmd = 'cat %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    with open(output, 'w') as o:
        for line in p.stdout:
            o.write(line)
    cmd_del = 'rm %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd_del, shell=True, stdout=subprocess.PIPE)
    return


def worker(num, source_chunk, target_list, output, vec_src, vec_trg, tm_file, n_best):
    """
    thread worker function
    searchs for cognates withing a window of frequencies
    it maximizes two features spelling and word embeddings
    """
    global target_sp
    global theta1
    global theta2
    print('Worker: %d'%num)
    print('num of words: %d'%len(source_chunk))
    worker_file = output + '.lex-tmp.' + str(num)
    out_tmp = open(worker_file, 'w+')
    
    #source_chunk = set(source_chunk)
    source_sp = Space.build(vec_src, source_chunk)
    #source_sp.normalize()
    if flag_tm:
        tm = np.loadtxt(tm_file)
        source_sp = apply_tm(source_sp, tm)
    
    #target_sp.normalize() 
    
    print 'Trans vectors: ', num
    print 'target words: ', len(target_list)
    
    for src in source_chunk:
        list_trg = []
        if src in source_sp.row2id:
            vec_src = source_sp.mat[source_sp.row2id[src]]
            for trg in target_list:
                key = src + ' ||| ' + trg
                if trg in target_sp.row2id:
                    vec_trg = target_sp.mat[target_sp.row2id[trg]]
                    cos = 1 - distance.cosine(vec_src[0], vec_trg[0])
                    dist = 1 - distlev.levenshtein(src, trg, normalized=True)
                    score = (theta1 * cos + theta2 * dist) / (theta1 + theta2)
                    list_trg.append((key, score))
                else:
                    score = 1 - distlev.levenshtein(src, trg, normalized=True)
                    list_trg.append((key, score))
        else:
            for trg in target_list:
                key = src + ' ||| ' + trg
                score = 1 - distlev.levenshtein(src, trg, normalized=True)
                list_trg.append((key, score))

        sort_trg = sorted(list_trg, key=lambda tup: tup[1], reverse=True)[:n_best]
        for (key, score) in  sort_trg:
            print >>out_tmp, '%s ||| %s'%(key, score)


    
    print('worker finished')
    return

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    #l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks


if __name__ == '__main__':
    if len(sys.argv) != 11:
        print 'usage:python lexicon_induction_bicvm.py <source-file> <target-file> <theta-1> <theta-2> <workers> <output> <vec-scr> <vec-trg> <tm-file> <n-best>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
