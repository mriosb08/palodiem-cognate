#commands prepro
#change input, pair and output pair for a different lang pair
#this script splits Opensubs into dev (1K sentences), test (2k sentences) and training (1M sentences)
IN="en"
OUT="es"
PAIR="en-es"
NAME="zoo"
N=`wc -l $NAME.$PAIR.$IN | cut -d' ' -f1`
echo $N
#dev
sed -n '1,1000p' $NAME.$PAIR.$IN > dev/$NAME.dev.$PAIR.$IN
sed -n '1,1000p' $NAME.$PAIR.$OUT > dev/$NAME.dev.$PAIR.$OUT
#test
sed -n '1001,2000p' $NAME.$PAIR.$IN > test/$NAME.test.$PAIR.$IN
sed -n '1001,2000p' $NAME.$PAIR.$OUT > test/$NAME.test.$PAIR.$OUT
#training
#echo $N
sed -n '2001,'$N'p' $NAME.$PAIR.$IN > training/$NAME.training.$PAIR.$IN
sed -n '2001,'$N'p' $NAME.$PAIR.$OUT > training/$NAME.training.$PAIR.$OUT
