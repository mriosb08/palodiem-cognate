import os
import dill
import sys
from collections import defaultdict

def tree():
        return defaultdict(tree)

def main(argv):
    """
    Reads zoo-name space (tree of subtitle files) and outputs parallel data with cdec format
    It uses simple heuristics to filter empty subtitles
    The source and target are languages from the moses zoo mapping.
    For example, es - Spanish, pt - Portuguese
    """
    (pickle_file, source, target) = argv
    zoo_namespace = dill.load(open(pickle_file, "rb"))
    s = dict()
    t = dict()
    en = dict()

    for title in zoo_namespace.iterkeys():
        for video in zoo_namespace[title].iterkeys():
            for lang in zoo_namespace[title][video].iterkeys():
                if zoo_namespace[title][video][lang]['num_lines'] != 0:
                    if lang.startswith(source):
                        s[video] = load_file(zoo_namespace[title][video][lang]['path'])
                    if lang.startswith(target):
                        t[video] = load_file(zoo_namespace[title][video][lang]['path'])
                    #if lang.startswith('en'):
                        #print lang
                        #en[video] = load_file(zoo_namespace[title][video][lang]['path'])

    for key in s.iterkeys():
        if key in t:
            if len(s[key]) == len(t[key]):
                for i,j in zip(s[key], t[key]):
                    if i and j:
                        #print e, '\t', i, '\t', j
                        print '%s ||| %s'%(i, j)
    return

def load_file(file_name):
    return [line.strip() for line in open(file_name)]

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python tr_zoo_v1.py <zoo-pickle> <source> <target>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

