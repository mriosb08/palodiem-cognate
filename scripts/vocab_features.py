import sys
from collections import defaultdict
from operator import itemgetter
import distance
import wagnerfischer
def main(args):
    (vocab_file, num_stem, threshold) = args
    with open(vocab_file) as vf:
        src_words = defaultdict(list)
        for line in vf:
            line = line.strip()
            (source, target, score) = line.split()
            ed1 = distance.levenshtein(source, target, normalized=True)
            (ed2, ed3, ed4) = wagnerfischer.levenshtein_ids(source, target)
            (src_root, src_flex) = split_token(source, int(num_stem))
            (trg_root, trg_flex) = split_token(target, int(num_stem))
            #print src_root, src_flex
            ed5 = distance.levenshtein(src_root, trg_root, normalized=True)
            ed6 = distance.levenshtein(src_flex, trg_flex, normalized=True)
            ed = [1.0-ed1, float(ed2), float(ed3), float(ed4), 1.0-ed5, 1.0-ed6]
            src_words[source].append((target, ed, score))

        for src, trg_list in src_words.iteritems():
            #is prob  of alignment high means high rank
            trg_sorted = sorted(trg_list, key=itemgetter(2), reverse=True)
            rank = 0
            for tup in trg_sorted:
                (trg, features, score) = tup
                ed1 = features[0]
                if ed1 < float(threshold):
                    continue
                print "%s\t%s\t%s\t%s"%(src, trg, rank , '\t'.join(map(str, features)))
                rank += 1
    return

def split_token(token, num_stem):
    try:
        root = token[:num_stem]
        flex = token[num_stem:]
        return (root, flex)
    except:
        return (token, token)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python vocab_features.py <vocab-file> <num-stem> <threshold>'
        sys.exit(1)
    else:
        main(sys.argv[1:])




