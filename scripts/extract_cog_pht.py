import sys
import pickle
import gzip
from collections import defaultdict
import distance
import string

def main(argv):
    (threshold, phrase_table1, phrase_table2) = argv
    added_phrases = []
       
    with gzip.open(phrase_table1) as pht1:
        for line1 in pht1:
            line1 = line1.strip()
            values1 = line1.split(' ||| ')
            s1_phrase= values1[0]
            t1_phrase= values1[1]
            with gzip.open(phrase_table2) as pht2:
                for line2 in pht2:
                    line2 = line2.strip()
                    values2 = line2.split(' ||| ')
                    s2_phrase= values2[0]
                    t2_phrase= values2[1]
                    flag = edit_distance(s1_phrase, s2_phrase, threshold)
                    if flag:
                        new_phrase = s1_phrase + ' ||| ' + t2_phrase + ' ||| ' + ' ||| '.join(values2[2:])
                        print new_phrase
                        #added_phrases.append(new_phrase)

    return

def edit_distance(s1, s2, threshold):
    """compares 2 phrases if they have token over the threshold they are marked as cognates"""
    #TODO add distributional feature

    threshold_count = 0
    s1 = s1.split()
    s2 = s2.split()
    ed = distance.levenshtein(s1, s2, normalized=True)
    #print s1, s2, ed
    if ed <= float(threshold):
        return True
    else:
        return False
    #for l1 in s1.split():
    #    for l2 in s2.split():
    #        ed = distance.levenshtein(l1, l2, normalized=True)
    #        if ed <= threshold:
    #            print ed, l1, l2, threshold
    #            threshold_count += 1
    return threshold_count

def del_punct(s):
    table = string.maketrans("","")
    return s.translate(table, string.punctuation)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python add_phrase_tables.py <threshold> <phrase-table1> <phrase-table2>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
