# -*- coding: utf-8 -*-
import sys
from bs4 import BeautifulSoup
import cdec
import gzip
#import codecs
from collections import OrderedDict
err = 0
def main(args):
    (input_file, weights, n_samples, out_type) = args
    segs = []
    global err
    decoder = cdec.Decoder(formalism='scfg')
    decoder.read_weights(weights)
    #index segments for order
    seg_file = open(input_file)
    data = seg_file.read()
    soup = BeautifulSoup(data)
    for seg in soup.findAll('seg'):
        id = int(seg['id'])
        segs.append((id, seg))
    
    #sort segments
    for id, seg in sorted(segs, key=lambda x: x[0]):
        grammar = seg['grammar']
        text = seg.string
        if out_type == 'nbest':
            translations = find_translation(decoder, text, grammar, int(n_samples))
            for translation in translations:
                print '%s ||| %s'%(str(id), translation.encode('utf-8'))
        elif out_type == 'sample':
            samples = find_samples(decoder, text, grammar, int(n_samples))
            for sample in samples:
                print '%s ||| %s'%(str(id), sample.encode('utf-8'))

    print >> sys.stderr, 'parse num error: %s'%(str(err))
    return

def find_samples(decoder, text, grammar_file, n_samples):
    samples = []
    if grammar_file.endswith('gz'):
        with gzip.open(grammar_file) as f:
                grammar = f.read()
    else:
        with open(grammar_file) as f:
                grammar = f.read()
    try:
        forest = decoder.translate(text, grammar=grammar)
        samples = forest.sample(n_samples)
    except:
        if text:
            print >> sys.stderr, 'parse error sampling: %s'%(text.encode('utf-8'))
        samples = [' ']
    return list(OrderedDict.fromkeys(samples))

def find_translation(decoder, text, grammar_file, n_translations):
    translations = []
    global err
    if grammar_file.endswith('gz'):
        with gzip.open(grammar_file) as f:
                grammar = f.read()
    else:
        with open(grammar_file) as f:
                grammar = f.read()
    try:
        forest = decoder.translate(text, grammar=grammar)
        translations = forest.kbest(n_translations)
    except:
        if text:
            print >> sys.stderr, 'parse error translation: %s'%(text.encode('utf-8'))
            err += 1
        translations = [' ']
    return list(OrderedDict.fromkeys(translations))
if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:python sample_cdec.py <seg-file> <weights-file> <n> <type>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
