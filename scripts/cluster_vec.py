# -*- coding: utf-8 -*-
import sys
import itertools
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import defaultdict
import pickle
#import codecs
def main(args):
    (src_file, src, trg_file, trg, n) = args
    (words_src, vec_src) = load_file(src_file, src)
    (words_trg, vec_trg) = load_file(trg_file, trg)
    words = words_src + words_trg
    X = np.vstack((vec_src, vec_trg))
    #print len(X)
    mbk =  MiniBatchKMeans(init='k-means++', n_clusters=int(n), batch_size=1000,
            n_init=10, max_no_improvement=10, random_state=0)

    pred = mbk.fit_predict(X)

    clus = defaultdict(list)
    pickle.dump(pred, open(src_file + '.pickle', "wb"))
    #print pred
    #print type(pred)
    #for i,p in enumerate(pred):
    #    print words[i],'\t', p
    for i,index in enumerate(pred):
        clus[index].append(words[i])
    for c in sorted(clus):
        print c,'\t', '\t'.join(clus[c])
    return

def load_file(input_file, input_type):
    words = []
    vec = []
    #print input_type
    with open(input_file) as inf:
        for line in inf:
            line = line.strip()
            cols = line.split(' ')
            words.append('%s|||%s'%(cols[0], input_type))
            vec.append([float(i) for i in cols[1:]])
    return(words, np.asarray(vec))

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print 'usage:python cluster_vec.py <vec_src> <src> <vec_trg> <trg> <N>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
