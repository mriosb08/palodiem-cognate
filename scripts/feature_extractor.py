# -*- coding: utf-8 -*-
import sys
import distance
import wagnerfischer
import pickle

def main(args):
    (lex_file, n_char) = args
    with open(lex_file) as lf:
        for line in lf:
            line =  line.strip()
            (source, f_src, target, f_trg, rank, score) = line.split('\t')
            (src_root, src_flex) = prepro_token(source, int(n_char))
            (trg_root, trg_flex) = prepro_token(target, int(n_char))
            ed1 = distance.levenshtein(source, target, normalized=True)
            (ed2, ed3, ed4) = wagnerfischer.levenshtein_ids(source, target)
            ed5 = distance.levenshtein(src_root, trg_root, normalized=True)
            ed6 = distance.levenshtein(src_flex, trg_flex, normalized=True)
            features = [1.0-ed1, float(ed2), float(ed3), float(ed4), 1.0-ed5, 1.0-ed6]
            print '%s\t%s\t%s\t%s'%(source, target, rank, '\t'.join(map(str, features)))
    return

def context(token, freq, cache_context):
    key = token + ':' + freq
    if key in cache_context:
        return cache_context[key]
    else:
        return ('','')


def prepro_token(token, num_stem):
    if len(token) == num_stem:
        return (token, token)
    try:
        root = token[:num_stem]
        flex = token[num_stem:]
        return (root, flex)
    except:
        return (token, token)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python feature_extractor.py <lex-file> <n-flex>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
