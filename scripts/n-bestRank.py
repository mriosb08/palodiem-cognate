import sys
from collections import defaultdict
import kenlm

def main(args):
    (n_file, lm_file, n) = args
    n_best = defaultdict(list)
    model = kenlm.LanguageModel(lm_file)
    n = int(n)
    with open(n_file) as nf:
        for line in nf:
            line = line.strip()
            cols = line.split('\t')
            try:
                id = int(cols[0])
                trg = cols[1]
                score = model.score(trg)
                n_best[id].append((trg, score))
            except:
                id = int(cols[0])
                n_best[id].append(('', 0.0))

    keylist = n_best.keys()
    keylist.sort()
    for key in keylist:
        best_l = sorted(n_best[key], key=lambda tup: tup[1])[:n]
        #best_l = set([x for x,y in best_l])
        for mt, ppl in best_l:
            print '%s\t%s\t%s'%(key, mt, str(ppl))

    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'python n-bestRank.py <n-best> <lm> <new-n>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
