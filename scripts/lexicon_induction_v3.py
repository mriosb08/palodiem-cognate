import sys
import codecs
import multiprocessing 
import distance
import wagnerfischer
import string
import operator

def worker(num, doc_list, n_best, num_stem, output):
    print 'worker: ', num, 'docs: ', len(doc_list)
    #worker_list = []
    worker_file = output + '.lex-tmp.' + str(num)
    out_tmp = open(worker_file, 'w+')
    table = string.maketrans("","")
    for (doc_scr, doc_trg) in doc_list:
        doc_scr = doc_scr.translate(table, string.punctuation)
        doc_trg = doc_trg.translate(table, string.punctuation)
        tokens_src = doc_scr.split() #assumption the docs are tokenized and lowercased
        tokens_trg = doc_trg.split()
        for source in tokens_src:
            tmp_scores = dict()
            if source.isdigit():
                continue
            for target in tokens_trg:
                #if source == target:
                #    continue
                #source = source.lower()
                #target = target.lower()
                ed1 = distance.levenshtein(source, target, normalized=True)
                (ed2, ed3, ed4) = wagnerfischer.levenshtein_ids(source, target)
                (src_root, src_flex) = prepro_token(source, num_stem)
                (trg_root, trg_flex) = prepro_token(target, num_stem)
                ed5 = distance.levenshtein(src_root, trg_root, normalized=True)
                ed6 = distance.levenshtein(src_flex, trg_flex, normalized=True)
                ed = [ed1, ed2, ed3, ed4, ed5, ed6]
                key = target + '|||' + '|||'.join(map(str, ed))
                tmp_scores[key] = ed1

            if len(tmp_scores) != 0:
                #print len(tmp_scores)
                #is edit distance lower means high rank
                sorted_scores = sorted(tmp_scores.items(), key=operator.itemgetter(1))
                for rank, tup in enumerate(sorted_scores[:n_best]):
                    (key_tup, score) = tup
                    features = key_tup.split('|||')
                    trg_key = features[0]
                    output = "%s\t%s\t%s\t"%(source, trg_key, rank) + '\t'.join(map(str, features[1:]))
                    #worker_list.append(output)
                    out_tmp.write(output + '\n')
    print 'worker finished: ', num
    return 

def prepro_token(token, num_stem):
    if len(token) == num_stem:
        return (token, token)
    try:
        root = token[:-num_stem]
        flex = token[-num_stem:]
        return (root, flex)
    except:
        return (token, token)

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    #l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks

def main(args):
    (doc_file, langs, n_jobs, n_best, n_stem, output) = args
    doc_list = []
    with open(doc_file) as df:
        for line in df:
            line = line.strip()
            #print line
            try:
                (src_doc, trg_doc) = line.split(' ||| ')
                doc_list.append((src_doc, trg_doc))
            except:
                continue
    n_jobs = int(n_jobs)
    jobs = []
    doc_chunks = chunks(doc_list, n_jobs)
    #pool = multiprocessing.Pool(processes=jobs)
    #results = [pool.apply_async(worker, args=(i, doc_chunks[i], n_best, )) for i in range(jobs)]
    #for worker_list in results:
    #    worker_list =  worker_list.get()
    #    for output in worker_list:
    #        print output
    for i in range(n_jobs):
        p = multiprocessing.Process(target=worker, args=(i, doc_chunks[i], int(n_best), int(n_stem), output))
        p.start()
        jobs.append(p)
    for j in jobs:
        j.join()
        #logger.info('%s.exitcode = %s' % (j.name, j.exitcode))
    cmd = 'cat %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    with open(output, 'w') as o:
        for line in p.stdout:
            print o.write(line)
    cmd_del = 'rm %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd_del, shell=True, stdout=subprocess.PIPE)
    return

if __name__ == '__main__':
    if len(sys.argv) != 7:
        print 'usage:python lexicon_induction_v3.py <doc-file> <src-trg> <num-jobs> <n-best> <num_stem> <output>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
