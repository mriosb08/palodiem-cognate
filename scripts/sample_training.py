import sys
import random

def main(args):
    """
    extract random pairs from a corpus
    input training corpus with cdec format
    size number of sentences to output
    """
    (training, size) = args
    list_training = [line.strip() for line in open(training)]
    output = random.sample(list_training,  int(size))
    for line in output:
        print line
    return

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python sample_training.py <training-corpus> <size>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
