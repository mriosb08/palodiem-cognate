# -*- coding: utf-8 -*-
import sys
import pickle
from gensim import corpora, models, similarities

def main(args):
    (file_name, model_file, output, no_freq) = args
    model= models.word2vec.Word2Vec.load_word2vec_format(model_file, binary=True)
    context = load_file(file_name, model, no_freq)
    pickle.dump(context, open(output, 'wb'))
    return

def load_file(file_name, model, no_freq):
    #TODO implement windows bigger than one
    context = dict()
    data = [line.strip() for line in open(file_name, 'r')]
    for i, line in enumerate(data):
        try:
            (fa, a) = data[i-1].split('\t')
            (fb, b) = data[i+1].split('\t')
            (fl, l) = line.split('\t')
            if a.lower().decode('utf-8') in model and b.lower().decode('utf-8') in model:
                vec_a = model[a.lower().decode('utf-8')]
                vec_b = model[b.lower().decode('utf-8')]
                if no_freq == '0':
                    key = l.lower()
                else:
                    key = l.lower() + ':' + fl
                context[key] = vec_a + vec_b
                #print key, context[key]
        except:
            print line
    return context

if __name__ == '__main__' :
    if len(sys.argv) != 5:
        print 'usage:python load_context_vec.py <lex-file> <bin-model> <output> <no-freq>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

