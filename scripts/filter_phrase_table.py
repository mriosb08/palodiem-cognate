import sys
import pickle
import gzip
from collections import defaultdict
import distance

def main(argv):
    (cog_file, lex_file, phrase-table) = argv
    added_phrases = []
    cf = load_lex(lex_file)
    with gzip.open(phrase-table) as pht:
        for line in pht:
            line = line.sptrip()
            (s_phrase, t_phrase, scores) = line.split(' ||| ')
            
            new_source = tr_cog(s_phrase, cf, lf)
            if new_source:
                new_phrase = new_source + ' ||| ' + t_phrase + ' ||| ' scores
                print new_phrase
                added_phrases.append(new_phrase)

    return

def tr_cog(phrase, cf, lf):
    new_phrase = phrase.split()
    pos_cog = None
    for i, token in enumerate(phrase.split()):
        if token in cf:
            pos_cog = i
            (new_phrase[i], score1, score2) = cf[token]
    if pos_cog:
        for i, token in enumerate(phrase.split()):
            if i != pos_cog and phrase[i] in :
                #TODO
                pass
        return ' '.join(new_phrase)
    else:
        return None


def load_cog(cog_file):
    cf_namespace = {}
    with open(cog_file) as cf:
        for line in cf:
            line = line.strip()
            (source, target, score1, score2) = line.split('\t')
            cf_namespace[target] = (source, float(score1), float(score2))
    return cf_namespace

def load_lex(lex_file):
    lf_namespace = defaultdict(list)
    with open(lex_file) as lf:
        for line in lf:
            line = line.strip()
            (source, target, score) = line.split('\t')
            lf_namespace[target].append((source, float(score)))
    return lf_namespace



if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python filter_phrase_table.py <filered-file> <phrase-table>'
        print 'TODO: not working yet!!!'
        sys.exit(1)
    else:
        main(sys.argv[1:])
