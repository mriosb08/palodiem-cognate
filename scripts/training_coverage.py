import sys
from collections import defaultdict, Counter
def main(args):
    """
    show training coverage 1-gram over test corpus (cdec format, note: use tokenized lowercased file) stats and list of OOV words
    input test corpus
    training corpus
    """
    (test_file, training_file) = args
    test = load_corpus(test_file)
    training = load_corpus(training_file)
    oov = []
    for word in test:
        if word not in training:
            oov.append(word)
            print word, '\t<OOV,%s>'%test[word]

    print >> sys.stderr, 'STDERR>> number of words in test: ', len(test)
    print >> sys.stderr, 'STDERR>> number of words in training: ', len(training)
    print >> sys.stderr, 'STDERR>> number of OOV words: ', len(oov) 


    return

def load_corpus(corpus_file):
    words = Counter()
    with open(corpus_file) as cf:
        for line in cf:
            line = line.strip()
            #print line
            try:
                (src, trg) = line.split(' ||| ')
            except:
                continue
            for token in src.split():
                words[token] += 1
    return words

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python wiki_coverage.py <test-corpus> <training-lex>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
