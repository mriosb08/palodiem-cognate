#steps to test a new document with cdec
#input:
#1) document, 2) moses ini 3) number size of n-best output
#1
if [[ $# -ne 3 ]] ; then
    echo 'usage: sh testMOSES_nbest.sh <source> <moses-ini> <n-best_size>'
    exit 1
fi
echo 'Preprocessing source' $1
/data/mrios/workspace/sw/cdec-2014-10-12/corpus/tokenize-anything.sh < $1 | /data/mrios/workspace/sw/cdec-2014-10-12/corpus/lowercase.pl 2>> $1.log 1> $1.lc

echo 'Decoding ' $1.lc
/data/mrios/workspace/sw/mosesdev/bin/moses \
-f $2  \
-n-best-list $1.lc.nbest $3 \
< $1.lc              \
> $1.lc.trans       \
2> $1.log
