#!/usr/bin/perl
my($xml_file, $src_lang, $trg_lang) = (shift, shift, shift);
#my $xml_file = $ARGV[0];
#my $langs = $ARGV[1];
#print "$xml_file, $src_lang, $trg_lang \n";
open(my $xml_in, $xml_file) or die "file not found\n";

sub clean_line{
    my $line = shift;
    $line =~ s/<[^>]*>//g;
    return $line;
}

sub bow{
    my @tokens = @_;
    return join(' ', @tokens);
}

my $dict = {};
my @src_doc = ();
my @tfg_doc = ();
my $flag_p = 0;
my $id = 0;
my $lang = '';

while(my $line = <$xml_in>){

    chomp($line);
    
    if($line =~m/<articlePair id=\"([0-9]+)\">/g){
        $id = $1;
    }
    if($line =~m/<article lang=\"([a-z]+)\" /g){
        $lang = $1;
    }
    if($line =~m/<content>/g){
        $flag_p++;
    }
    if($flag_p != 0){
        #print "$id, $lang, $src_lang, $trg_lang, $line\n";
        if($lang eq $src_lang){
            $n_line = clean_line($line);
            if($n_line !~ /^$/){
                push(@src_doc, $n_line);
            }
        }elsif($lang eq $trg_lang){
            $n_line = clean_line($line);
            if($n_line !~ /^$/){
                push(@trg_doc, $n_line);
            }
        }
    }
    if($line =~m/<\/articlePair>/g){
        $flag_p = 0;
        $tokens_src = bow(@src_doc);
        $tokens_trg = bow(@trg_doc);
        @src_doc = ();
        @trg_doc = ();
        #$dict{$id} = $tokens_src . ' ||| '. $tokens_trg;
        my $output = $tokens_src . ' ||| '. $tokens_trg;
        print "$output\n";        
    }

}
