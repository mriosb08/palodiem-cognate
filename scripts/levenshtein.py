
def levenshtein_w(s1, s2, w1, w2, w3):
    """
    Simple weighted levenshtein
    input: string 1, string 2 
    weights: w1 => insertions, 
    w2 => deletions
    w3 => substitutions
    
    """
    if len(s1) < len(s2):
        return levenshtein_w(s2, s1, w1, w2, w3)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 
            deletions = current_row[j] + 1       
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(w1*insertions, w2*deletions, w3*substitutions))
        previous_row = current_row
    distance = previous_row[-1] / float(len(s1))
    return distance

def levenshtein_numop(s1, s2):
    """
    Simple evenshtein
    input: string 1, string 2 
    output: num of operations    
    """
    if len(s1) < len(s2):
        return levenshtein_numop(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)
    previous_row = range(len(s2) + 1)
    
    isd_row = []
    
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    distance = previous_row[-1]
    return distance

