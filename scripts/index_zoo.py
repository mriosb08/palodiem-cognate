import sys
import os
import dill
import re
from collections import defaultdict

def tree():
        return defaultdict(tree)

def main(argv):
    zoo_namespace = tree()
    (zoo_path, file_mapping, pickle_file) = argv
    langs = defaultdict(int)
    zoo_mapping = load_mapping(file_mapping)
    for path, dirs, files in os.walk(zoo_path):
        for f in files:
                if f.startswith('.'):
                    continue
                num_lines = num_linesfile(path, f)
                (shortname, extension) = os.path.splitext(f)
                m = re.match(r"(video_\d+)_(.*)", shortname)
                if m:
                    (video, lang) = m.groups()
                    mlang = moses_namespace(lang, zoo_mapping)
                    title = path.split('/')[-1]
                    native = is_native(lang)
                    zoo_namespace[title][video][mlang]['num_lines'] = num_lines
                    zoo_namespace[title][video][mlang]['native'] = native
                    zoo_namespace[title][video][mlang]['lang'] = lang
                    zoo_namespace[title][video][mlang]['path'] = os.path.join(path, f)
                #print title, video, mlang, zoo_namespace[title][video][mlang].path
                #print zoo_namespace[title][video][mlang].num_lines
                    langs[lang] += 1
    dill.dump(zoo_namespace, open(pickle_file, 'wb'))
    print_langs(langs)
    return

def load_mapping(file_mapping):
    """load file with zoo- moses mapping"""
    zoo_mapping = {}
    with open(file_mapping) as fm:
        for line in fm:
            line = line.strip()
            print line
            (zoo, moses) = line.split('\t')
            zoo_mapping[zoo] = moses
    return zoo_mapping


def print_langs(langs):
    """print langs and each text count"""
    for lang, count in langs.iteritems():
        print lang,'\t',count
    return

def moses_namespace(lang, zoo_mapping):
    """maping from zoo lang names to moses"""
    lang = lang.replace("_(native)", "")
    lang = lang.replace("Production_", "")
    if lang in zoo_mapping:
        return zoo_mapping[lang]
    elif lang == '#' or lang not in zoo_mapping:
        lang = lang.replace(" ", "_")
        lang = lang.lower()
        return lang

def num_linesfile(path, file):
    """number of lines in file"""
    num_lines = sum(1 for line in open(os.path.join(path, file)) if line.strip())
    return num_lines

def is_native(lang):
    """is language native of the video"""
    native = None
    if 'native' in lang:
        native = True
    else:
        native = False
    return native

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python index_zoo.py <path-to-zoo> <zoo-moses-mapping> <picklefile>'
        sys.exit(0)
    else:
        main(sys.argv[1:])

