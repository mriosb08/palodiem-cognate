#steps to test a new document with cdec
#input:
#1) document, 2) moses ini
#1
if [[ $# -ne 2 ]] ; then
    echo 'usage: sh testMOSES.sh <source> <moses-ini>'
    exit 1
fi
echo 'Preprocessing source' $1
/data/mrios/workspace/sw/cdec-2014-10-12/corpus/tokenize-anything.sh < $1 | /data/mrios/workspace/sw/cdec-2014-10-12/corpus/lowercase.pl 2>> $1.log 1> $1.lc

echo 'Decoding ' $1.lc
/data/mrios/workspace/sw/mosesdev/bin/moses \
-f $2  \
< $1.lc              \
> $1.lc.trans         \
2> $1.log
