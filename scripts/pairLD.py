import sys
import distance
import codecs 

def main(args):
    output = codecs.open(args[1], 'w', 'utf-8')
    with open(args[0]) as inf:
        for line in inf:
            line = line.strip()
            (src, trg) = line.split(' ')
            score = distance.levenshtein(unicode(src, encoding='utf-8').lower(), unicode(trg, encoding='utf-8').lower(), normalized=True)
            print >>output, '%s\t%s\t%s'%(unicode(src, encoding='utf-8').lower(), unicode(trg, encoding='utf-8').lower(), score)
    return

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python pairLD.py <input-file> <output-file>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
