import argparse
import numpy as np
#import pylab as pb
import GPy
import sys
import re
import time
import dill
def main():
    """
    uses multitask coregionalization kernel over sereval feature files (tasks/domains)
    trains and tests.
    """
    parser = argparse.ArgumentParser(description='python gpy_sparse_mtl_test.py')
    parser.add_argument('-t', action="store", dest='task',
            help='Training model', required=True)
    parser.add_argument('-i', action="store", dest='test',
            help='test dataset', required=True)
    parser.add_argument('-o', action="store", dest='output',
            help='prediction output', required=True)
    parser.add_argument('-n', action="store", dest='t_batch',
            help='number of batches to split the testing', required=True)
    parser.add_argument('-d', action="store", dest='test_task',
            help='number of test task', required=True)
    parser.add_argument('-l', action="store", dest='max_rank',
            help='max number of ranking exemples for testing', type=int, default=10)
    
    results = parser.parse_args()

    model_file = results.task
    test_file = results.test
    output_file = results.output
    n = int(results.t_batch)
    test_task = results.test_task
    max_rank = results.max_rank
    (X_test, y_test, test_pairs) = load_file([test_file], test_task, max=max_rank)
    m = dill.load(open(model_file, "rb"))
    start = time.time()
    x_list = chunks(X_test, n)
    pair_list = chunks(test_pairs, n)
    for x, pair in zip(x_list, pair_list):
        (Yp, Vp) = m.predict(x)
        for tup, pred in zip(pair, Yp):
            (a, b) = tup
            print a, '\t', b, '\t', pred
    return

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def load_file(feature_files, id_t=None, max=10):
    """
    load feature file with format: src target rank featue1 feature2 .. featureN
    """
    pairs = []
    X = []
    y = []
    id = []
    
    for task, ff in enumerate(feature_files):
        with open(ff) as f:
            for line in f:
                line = line.strip()
                #print line
                #p = re.compile(r'\t')
                cols = line.split('\t')
                pairs.append((cols[0], cols[1]))
                rank = cols[2]
                if int(rank) > max:
                    continue
                if id_t:
                    id_task = id_t
                else:
                    id_task = task 
                features = cols[3:]
                features.append(id_task)
                y.append([float(rank)])
                X.append([float(x) for x in features])
    
    #print y
    #print len(y)
    y = np.asarray(y)
    X = np.asarray(X)
    return (X, y, pairs)

if __name__ == '__main__':
    main()
