import multiprocessing
import sys
from more_itertools import chunked
import distance
from collections import defaultdict
import re
import tempfile
import subprocess
import wagnerfischer
import operator

def worker(num, source_chunk, target_list, threshold, window, n_best):
    """
    thread worker function
    searchs for a cognate in parallel (number of workers) within a window of frequencies.
    threshold only search for words over a certain frequency.
    """
    print 'worker', num, len(source_chunk), len(target_list), threshold, window, n_best
    worker_list = []
    for (src_features, source_freq) in source_chunk:
        (source, src_root, src_flex) = src_features
        tmp_scores = dict()
        a = source_freq - window
        b = source_freq + window
        for bin in xrange(a, b):
            if bin in target_list:
                chunk_target = target_list[bin]
                for trg_features in chunk_target:
                    (target, trg_root, trg_flex) = trg_features
                    if source == target:
                        continue
                    ed1 = distance.levenshtein(source, target, normalized=True)
                    (ed2, ed3, ed4) = wagnerfischer.levenshtein_ids(source, target)
                    ed5 = distance.levenshtein(src_root, trg_root, normalized=True)
                    ed6 = distance.levenshtein(src_flex, trg_flex, normalized=True)
                    #sim1 = abs(source_freq - bin)
                    ed = [ed1, ed2, ed3, ed4, ed5, ed6]
                    key = target + '|||' + '|||'.join(map(str, ed))
                    tmp_scores[key] = sum(ed) / float(len(ed))

        if len(tmp_scores) != 0:
            #is edit distance lower means high rank
            sorted_scores = sorted(tmp_scores.items(), key=operator.itemgetter(1))
            for rank, tup in enumerate(sorted_scores[:n_best]):
                (key_tup, score) = tup
                features = key_tup.split('|||')
                trg_key = features[0]
                output = "%s\t%s\t%s\t"%(source, trg_key, rank) + '\t'.join(map(str, features[1:]))
                worker_list.append(output)
    return worker_list

def load_source(source_file, threshold, num_stem):
    """
    loads source file 
    cleans numbers and links
    """
    chunk = dict()
    with open(source_file) as sf:
        for line in sf:
            line = line.strip()
            (source_freq, source) = line.split('\t')
            #skip numbers and links
            if re.search(r'\d+', source) or re.search('http', source):
                continue
            if int(source_freq) < threshold:
                continue
            src_features = prepro_token(source.lower(), num_stem)
            chunk[source.lower()] = (src_features, int(source_freq))
    return chunk

def prepro_token(token, num_stem):
    if len(token) == num_stem:
        return (token, token, token)
    try:
        root = token[:num_stem]
        flex = token[num_stem:]
        return (token, root, flex)
    except:
        return (token, token, token)

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks


if __name__ == '__main__':
    if len(sys.argv) != 9:
        print 'usage:python lexicon_induction_v2.py <source-file> <target-file> <n-best> <threshold> <workers> <window> <output> <num-stem>'
        sys.exit(1)
    source_file = sys.argv[1]
    target_file = sys.argv[2]
    n_best= int(sys.argv[3])
    threshold = int(sys.argv[4])
    num_jobs = int(sys.argv[5])
    window = int(sys.argv[6])
    output = sys.argv[7]
    num_stem = int(sys.argv[8])
    source_words = load_source(source_file, threshold, num_stem)
    source_chunks = chunks(source_words, num_jobs)
    jobs = []
    target_list = defaultdict(list)
    
    with open(target_file) as tf:
        for line in tf:
            line = line.strip()
            (target_freq, target) = line.split('\t')
            if int(target_freq) < threshold:
                continue
            trg_features = prepro_token(target.lower(), num_stem)
            target_list[int(target_freq)].append(trg_features)
    pool = multiprocessing.Pool(processes=num_jobs)
    out = open(output, 'w')
    results = [pool.apply_async(worker, args=(i, source_chunks[i], target_list, threshold, window, n_best)) for i in range(num_jobs)]
    for worker_list in results:
        worker_list =  worker_list.get()
        for line in worker_list:
            print >>out, line
