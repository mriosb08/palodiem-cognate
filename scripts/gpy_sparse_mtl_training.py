import argparse
import numpy as np
#import pylab as pb
import GPy
import sys
import re
import time
import dill
def main():
    """
    uses multitask coregionalization kernel over sereval feature files (tasks/domains)
    trains and tests.
    """
    parser = argparse.ArgumentParser(description='python gpy_sparse_mtl_training.py')
    parser.add_argument('-t', action="append", dest='tasks',
            help='Training datasets', required=True)
    parser.add_argument('-o', action="store", dest='output',
            help='model output', required=True)
    parser.add_argument('-n', action="store", dest='n_tasks',
            help='number of training tasks', required=True)
    parser.add_argument('-p', action="store", dest='induce_points',
            help='number of induce points', required=True)
    parser.add_argument('-m', action="store", dest='max_iters',
            help='max number of iterations for training', required=True)
    parser.add_argument('-l', action="store", dest='max_rank',
            help='max number of ranking exemples for training', type=int, default=10)
    
    results = parser.parse_args()

    train_files = results.tasks
    pickle_file = results.output + '.pickle'
    num_tasks = results.n_tasks
    n = results.induce_points
    max_rank = results.max_rank
    iter = results.max_iters
    (X_train, y_train, training_pairs) = load_file(train_files, max=max_rank)
    dim = len(X_train[0])
    start = time.time()

    kern_coreg = GPy.kern.RBF(input_dim=(dim-1))**GPy.kern.Coregionalize(1, output_dim=int(num_tasks), rank=1)
    m = GPy.models.SparseGPRegression(X_train, y_train, kernel=kern_coreg, num_inducing=int(n)) #all dataset

    m.optimize('bfgs', max_iters=iter)
    
    end = time.time()
    print 'training time', end - start
    dill.dump(m, open(pickle_file, 'wb')) 
    print m
    print m.log_likelihood()
    return

def load_file(feature_files, id_t=None, max=10):
    """
    load feature file with format: src target rank featue1 feature2 .. featureN
    """
    pairs = []
    X = []
    y = []
    id = []
    
    for task, ff in enumerate(feature_files):
        with open(ff) as f:
            for line in f:
                line = line.strip()
                #print line
                #p = re.compile(r'\t')
                cols = line.split('\t')
                pairs.append((cols[0], cols[1]))
                rank = cols[2]
                if int(rank) > max:
                    continue
                if id_t:
                    id_task = id_t
                else:
                    id_task = task 
                features = cols[3:]
                features.append(id_task)
                y.append([float(rank)])
                X.append([float(x) for x in features])
    
    #print y
    #print len(y)
    y = np.asarray(y)
    X = np.asarray(X)
    return (X, y, pairs)

if __name__ == '__main__':
    main()
