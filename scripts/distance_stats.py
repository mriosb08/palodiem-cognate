import distance
import sys
import numpy as np


def main(argv):
    (distance_name, lex_file) = argv
    #distance_names = {'levenshtein':1
    #        , 'hamming':2
    #        , 'jaccard':3
    #        , 'sorensen':4}
    tmp_scores = []
    with open(lex_file) as lf:
        for line in lf:
            line = line.strip()
            (source, target, score) = line.split()
            edit_distance = 1.0
            if distance_name == 'levenshtein':
                edit_distance = distance.levenshtein(source, target, normalized=True)
                #print source, target, result
            elif distance_name == 'hamming':
                edit_distance = distance.hamming(source, target)
            elif distance_name == 'jaccard':
                edit_distance = distance.jaccard(source, target)
            else:
                edit_distance = distance.sorensen(source, target)
                
            tmp_scores.append(edit_distance)
        distance_scores = np.asarray(tmp_scores)
        print 'min: ', np.amin(distance_scores)
        print 'max: ', np.amax(distance_scores)
        print 'median:', np.median(distance_scores)
        print 'mean: ', np.mean(distance_scores)
        print 'average: ', np.average(distance_scores)
        print 'std: ', np.std(distance_scores)
        print 'var:', np.var(distance_scores)
        print 'histo: ', np.histogram(distance_scores)
    return


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage: python distance_stats.py <distance> <lex-file-moses>'
        sys.exit(0)
    else:
        main(sys.argv[1:])
