import os
import dill
import sys
from collections import defaultdict

def tree():
        return defaultdict(tree)

def main(argv):
    """
    Reads zoo-name space (tree of subtitle files) and outputs parallel data with cdec format
    It uses simple heuristics to filter empty subtitles
    The source and target are languages from the moses zoo mapping.
    For example, es - Spanish, pt - Portuguese
    """
    (pickle_file, source, target, title, video) = argv
    zoo_namespace = dill.load(open(pickle_file, "rb"))
    s = dict()
    t = dict()
    en = dict()
    
    s[video] = load_file(zoo_namespace[title][video][source]['path'])
    t[video] = load_file(zoo_namespace[title][video][target]['path'])
    #en[video] = load_file(zoo_namespace[title][video]['en_us']['path'])

    for key in s.iterkeys():
        if key in t:
            for i,j in zip(s[key], t[key]):
                    if i and j:
                        print >>sys.stdout, i, ' ||| ', j
                        #print >>sys.stderr, k
    return

def load_file(file_name):
    print >>sys.stderr, file_name
    return [line.strip() for line in open(file_name)]

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print 'usage:python tr_zoo_v2.py <zoo-pickle> <source> <target> <title> <video>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

