from sklearn.cluster import DBSCAN
import numpy as np
import sys
import distance

def main(argv):
    """
    cluster words by edit distance score
    input is the lex induction output file with scores
    the user defines the number of scores
    dependency: scikit learn
    jobs: run in parallel
    """
    (lex_file, num_clus, num_jobs) = argv
    (X, pairs) = extract_features(lex_file)
    estimator = DBSCAN()
    labels = estimator.fit_predict(X)
    for pair,label in zip(pairs, labels):
        print pair, label
    return

def extract_features(input_file):
    """
    loads scores from lex file
    """
    X = []
    pairs = []
    with open(input_file) as input:
        for line in input:
            line = line.strip()
            #(a, a_freq, b, b_freq, score) = line.split('\t')
            cols = line.split('\t')
            a = cols[0]
            b = cols[1]
            score = cols[2]
            features = cols[3:]
            X.append([float(f) for f in features])
            pairs.append((a,b))
    return (X, pairs)

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage: python edit_distance_cluster_dp.py <lex_file>'
        sys.exit(1)
    main(sys.argv[1:])
