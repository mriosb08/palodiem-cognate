#commands prepro
#splits opensubs or zoo (cdec format)  into test (2K sentences), dev (1K sentences), and training (remaining)
PAIR="uk-ru"
NAME="opensub"
N=`wc -l $NAME.$PAIR | cut -d' ' -f1`
echo $N
#dev
sed -n '1,1000p' $NAME.$PAIR > dev/$NAME.dev.$PAIR
#test
sed -n '1001,3000p' $NAME.$PAIR > test/$NAME.test.$PAIR
#training
#echo $N
sed -n '3001,'$N'p' $NAME.$PAIR > training/$NAME.training.$PAIR
