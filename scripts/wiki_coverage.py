import sys
from collections import defaultdict, Counter
def main(args):
    """
    show wiki coverage 1-gram over test corpus (cdec format, note: use tokenized lowercased file) stats and list of OOV words
    input test corpus
    wikipedia lex format from CWB!!
    """
    (test_corpus, wiki_file) = args
    words = load_corpus(test_corpus)
    wiki = load_wiki(wiki_file)
    oov = []
    for word in words:
        if word not in wiki:
            oov.append(word)
            print word, '\t<OOV,%s>'%words[word]

    print >> sys.stderr, 'STDERR>> number of words in test: ', len(words)
    print >> sys.stderr, 'STDERR>> number of words in wiki: ', len(wiki)
    print >> sys.stderr, 'STDERR>> number of OOV words: ', len(oov) 


    return

def load_wiki(wiki_file):
    wiki = defaultdict(int)
    with open(wiki_file) as wf:
        for line in wf:
            line = line.strip()
            (freq, word) = line.split('\t')
            wiki[word.lower()] = int(freq)
    return wiki

def load_corpus(corpus_file):
    words = Counter()
    with open(corpus_file) as cf:
        for line in cf:
            line = line.strip()
            (src, trg) = line.split(' ||| ')
            for token in src.split():
                words[token] += 1
    return words

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python wiki_coverage.py <test-corpus> <wiki-lex>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
