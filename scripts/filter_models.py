import pickle
from scipy.spatial import distance

class Filter(object):
    """
    filter base class it allows to configure different types for filtering
    to quit noise from the output of lexicon induction files
    """
    def __init__(self):
        return

    def configure(self, config_file):
        """
        read a config file for a model
        the format is: key value
        for example, 
        threshold   0.2
        """
        self.config = dict()
        with open(config_file) as cf:
            for line in cf:
                line = line.strip()
                if line.startswith('#'):
                    continue
                (key, value) = line.split()
                self.config[key] = value


class ThresholdScore(Filter):
    """
    filter the file given a score threshold
    from file 
    if the score is below the theshold the pair is keeped
    for edit distance
    """
    def __init__(self):
        super(ThresholdScore, self).__init__()
        return

    def initialize(self):
        self.threshold = float(self.config['threshold'])
        return

    def process_entry(self, a, fa, b, fb, score):
        if score <= self.threshold:
            return True
        else:
            return False



class ThresholdContext(Filter):
    """
    filter the file based on context cos sim between contexts based on freq
    """
    def __init__(self):
        super(ThresholdScore, self).__init__()
        return

    def initialize(self):
        self.threshold = float(self.config['threshold'])
        self.cache_src = pickle.load(open(self.config['chache_src'], 'rb'))
        self.cache_trg = pickle.load(open(self.config['chache_trg'], 'rb'))
        return

    def process_entry(self, a, fa, b, fb, score):
        keya = a + ':' + fa
        keyb = b + ':' + fb
        if keya in self.cache_src and keyb in self.cache_trg:
            veca = self.cache_src[keya]
            vecb = self.cache_trg[keyb]
            cos_vec = distance.cosine(veca, vecb)
            if cos_vec < self.threshold:
                return False
            else:
                return True

        else:
            return True


 class ThresholdFreq(Filter):
    """
    filter the file based on freq diff
    """
    def __init__(self):
        super(ThresholdScore, self).__init__()
        return

    def initialize(self):
        self.threshold = float(self.config['threshold'])
        return

    def process_entry(self, a, fa, b, fb, score):
        diff = abs(fa -fb)
        if diff <= theshold:
            return True
        else:
            return False

