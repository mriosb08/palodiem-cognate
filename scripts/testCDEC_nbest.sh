#steps to test a new document with cdec
#input:
#1) document, 2) path to training ini, 2) path to cdec decode ini, 3) path to cdec weights, 4) path to grammar
#5) k best size
#1
if [[ $# -ne 7 ]] ; then
    echo 'usage: sh testCDEC.sh <text> <path-training> <path-decode> <path-weights> <path-grammar> <n-best-size> <lang-pair>'
    exit 1
fi
echo 'Preprocessing input' $1
/data/mrios/workspace/sw/cdec-2014-10-12/corpus/tokenize-anything.sh < $1 | /data/mrios/workspace/sw/cdec-2014-10-12/corpus/lowercase.pl 2>> $1.log 1> $1.$7.lc
#2
echo 'Extracting grammar into' $1.$7.sgm
python -m cdec.sa.extract -c $2 -g $5 -j 10 -z < $1.$7.lc 1> $1.$7.sgm 2>> $1.$7.log
#3
echo 'Decoding with grammar' $1.sgm 'with weigth' $3 'into' $1.trans
/data/mrios/workspace/sw/cdec-2014-10-12/decoder/cdec -c $3 -w $4 -i $1.$7.sgm -k $6 2>> $1.log 1> $1.$7.nbest.trans
