# -*- coding: utf-8 -*-
import sys
import pickle

def main(args):
    file_name, window, output, freq, threshold = args
    context = load_file(file_name, int(window), int(freq), int(threshold))
    #pickle.dump(context, open(output, 'wb'))
    return

def load_file(file_name, window, freq, threshold):
    #TODO implement windows bigger than one
    context = dict()
    data = [line.strip() for line in open(file_name, 'r')]
    for i, line in enumerate(data):
        try:
            #(fa, a) = data[i-1].split('\t')
            #(fb, b) = data[i+1].split('\t')
            w1 = i - window
            w2 = i + window
            a = [t.lower().split('\t')[1] for t in data[w1:i]]
            b = [t.lower().split('\t')[1] for t in data[i+1:w2+1]]
            (fl, l) = line.split('\t')
            if int(fl) < threshold:
                continue
            if freq == 1:
                key = l.lower() + ':' + fl
            else:
                key = l.lower()
            context[key] = (a, b)
            print '%s %s %s'%(' '.join(a), l.lower(),' '.join(b))
        except:
            #print line
            pass
    return context

if __name__ == '__main__' :
    if len(sys.argv) != 6:
        print 'usage:python load_context.py <lex-file> <window> <output> <freq> <thresold>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

