import goslate
import sys

def main(args):
    """
    uses goslate to trasnlate with google api
    http://pythonhosted.org/goslate/#module-goslate
    input: parallel corpus cdec format, source and target languages
    """
    (input, src, trg) = args
    gs = goslate.Goslate()
    input_sent = []
    with open(input) as corpus:
        for line in corpus:
            #line = line.strip()
            #(source, target) = line.split(' ||| ')
            source = line.strip()
            input_sent.append(source)
        trans = gs.translate(input_sent, trg, src)
        for tr in trans:
            print  source, '\t', tr.encode('utf-8')
    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python <parallel-corpus> <source> <target>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

