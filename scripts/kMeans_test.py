import sys
from sklearn.cluster import MiniBatchKMeans
import pickle
import numpy as np
import re
from collections import defaultdict
import distance

def main(args):
    (features_file, trg_tok, n, clus_pickle) = args
    n = int(n)
    
    (src_tok, X_test) = load_feat(features_file)
    trg_tok =  pickle.load(open(trg_tok, "rb")) 
    mbk = pickle.load(open(clus_pickle, "rb"))
    trg_labels = mbk.labels_
    clus = defaultdict(list)

    for trg, label in zip(trg_tok, trg_labels):
        clus[label].append(trg)

    pred = mbk.predict(X_test)
    
    for (src, src_label) in zip(src_tok, pred):
        pairs = []
        if src_label in clus:
            for trg in clus[src_label]:
                d =  1 - distance.levenshtein(src, trg, normalized=True)
                pairs.append((trg, d))
            pairs_sorted = sorted(pairs, key=lambda tup: tup[1], reverse=True)
            for t, s in pairs_sorted[:n]:
                print '%s ||| %s ||| %s'%(src, t, s)
    return

def load_feat(feat_file):
    feat = []
    words = []
    i = 0
    with open(feat_file) as features:
        for line in features:
            line = line.strip()
            cols = re.split('\s+', line)
            words.append(cols[0])
            features = [float(col) for col in cols[1:]]
            feat.append(features)
            i += 1
    return (words, np.asarray(feat))

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:python kMeans_test.py <src-feat> <trg-tok> <n-best> <clus-pickle>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

