# -*- coding: utf-8 -*-
import sys
from collections import defaultdict

def main(args):
    (file_src, file_trg, pred_nbest, at) = args
    at = int(at)
    gold_src = [line.strip() for line in open(file_src)]
    gold_trg = [line.strip() for line in open(file_trg)]
    pred_cog = load_pred(pred_nbest)
    sum = 0
    tot = 0
    for s, t in zip(gold_src, gold_trg):
        if s in pred_cog:
            top = pred_cog[s]
            if t in top[:at]:
                sum += 1
            tot += 1
    sum = sum / float(tot)
    print 'acc: ', sum
      

def load_pred(pred_nbest):
    pred = defaultdict(list)
    with open(pred_nbest) as nbest:
        for line in nbest:
            line = line.strip()
            cols = line.split(' ||| ')
            pred[cols[0]].append(cols[1])
    return pred

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage: python eval_coglist.py <src-gold> <trg-gold> <pred-nbest> <at>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
