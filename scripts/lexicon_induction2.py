import sys
import distance
import pyphen

def main(argv):
    """
    Exhaustive search for cognates on wikipedia (slow)
    The unit to compare editions are syllables.
    the source target are languages.
    For example, es-pt, for Spanish and Portuguese
    """
    (source_file, target_file, lang_pair, threshold) = argv
    (source_lang, target_lang) = lang_pair.split('-')
    source_dict = pyphen.Pyphen(lang=source_lang)
    target_dict = pyphen.Pyphen(lang=target_lang)

    separator = '|||'
    threshold = int(threshold)
    source_tokens = load_file(source_file, source_dict, separator)
    target_tokens = load_file(target_file, target_dict, separator)
    print 'LOADED'
    for s, s_values in source_tokens.iteritems():
        tmp_scores = dict()
        (s_hypen, s_freq) = s_values
        for t, t_values in target_tokens.iteritems():
            (t_hypen, t_freq) = s_values
            if s_freq >= threshold and t_freq >= threshold:
                tmp_scores[t] = levenshtein(s_hypen.split(separator), t_hypen.split(separator))
                print s_hypen.split(separator), t_hypen.split(separator), tmp_scores[t]
            #pick the min score
        if len(tmp_scores) != 0:
            (target_cognate, score) = min(tmp_scores.iteritems(), key=lambda tmp_scores: tmp_scores[1])  
            print source,'\t', target_cognate, '\t', score
    return

def load_file(token_file, hyp_dict, separator):
    tokens = dict()
    with open(token_file) as tf:
        for line in tf:
            line = line.strip()
            (freq, token) = line.split('\t')
            token_hypen = hyp_dict.inserted(token.decode('utf-8'), hyphen=separator)
            tokens[token] = (token_hypen, int(freq))

    return tokens

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)
    previous_row = range(len(s2) + 1)
    
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1] / float(len(s1))


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage: python lexicon_induction2.py <source-cwb> <target-cwb> <source-target> <freq-threshold>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
