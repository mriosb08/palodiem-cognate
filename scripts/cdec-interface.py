#coding: utf8
import sys
import cdec
import gzip
from bs4 import BeautifulSoup
import codecs

def main(args):
    """
    interfce to cdec to translate input sgm files
    note: use when plug a new feature fuction
    weights is the output from tune mira note: weights.final
    option k-best
    number of ouput translations
    TODO:TEST!!!
    """
    (w_file, input_file, k_best) = args
    k_best = int(k_best)
    in_file = codecs.open(input_file, 'r', 'utf8')
    data = in_file.read()
    soup = BeautifulSoup(data)
    decoder = cdec.Decoder(formalism='scfg')
    decoder.read_weights(w_file)

    for seg in soup.findAll('seg'):
        src = seg.string
        grammar_file = seg['grammar']
        id = '[' + seg['id'] + ']'
        if grammar_file.endswith('.gz'):
            with gzip.open(grammar_file) as f:
                grammar = f.read()
        else:
            with open(grammar_file) as f:
                grammar = f.read()
        #print src
        #print grammar
        try:
            forest = decoder.translate(src, grammar=grammar)
        except:
            print id, ' ||| ' ,src, ' ||| <unk>'
            continue

        if k_best == 1:
            print id,' ||| ' ,src, ' ||| ', forest.viterbi().encode('utf8')
        else:
            kbest_tr = [i for i in forest.kbest(k_best)]
            print id, ' ||| ', src, ' ||| '.join(kbest_tr)
    #sgm.close()
    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python cdec-interface.py <weight-file> <input-file> <k-best>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
