import os
import dill
import sys
from collections import defaultdict

def tree():
        return defaultdict(tree)

def main(argv):
    """
    Reads zoo-name space (tree of subtitle files) and outputs parallel data with cdec format
    It uses simple heuristics to filter empty subtitles
    The source and target are languages from the moses zoo mapping.
    For example, es - Spanish, pt - Portuguese
    """
    (pickle_file) = argv[0]
    zoo_namespace = dill.load(open(pickle_file, "rb"))
    s = dict()
    langs = []


    for title in zoo_namespace.iterkeys():
        for video in zoo_namespace[title].iterkeys():
            n = len(zoo_namespace[title][video])
            key = video + '|||' + title
            for lang in zoo_namespace[title][video].iterkeys():
                langs.append((n,lang, key, zoo_namespace[title][video][lang]['path']))

    for (n, lang, key, path) in sorted(langs, key=lambda tup: tup[0], reverse=True):
        print n, lang, key, path 

    return

def load_file(file_name):
    return [line.strip() for line in open(file_name)]

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'usage:python extract_test_zoo.py <zoo-pickle>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

