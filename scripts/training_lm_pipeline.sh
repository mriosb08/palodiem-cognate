INIT=$1
INPUT=$2
N=$3
ORDER=$4
OUTPUT=$5

echo "Proc n-best"
/data/mrios/workspace/sw/mymoses/bin/moses -f $INIT -n-best-list $OUTPUT.nbest $N -output-unknowns $OUTPUT.oov < $INPUT > $OUTPUT.trans 2> $OUTPUT.log
echo "Training per segment LM"
python /data/mrios/workspace/palodiem-cognate/nlm/train_lm.py $OUTPUT.nbest $ORDER $OUTPUT.pickle
