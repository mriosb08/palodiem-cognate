import sys
import wikipedia
import string
import re

def main(args):
    (title_file, lang_pair) = args
    (src_lang, trg_lang) = lang_pair.split('-')

    with open(title_file) as tf:
        for line in tf:
            line = line.strip()
            try:
                (src_title, trg_title) = line.split(' ||| ')
            except:
                continue
            src_content = extract_content(src_title, src_lang)
            trg_content = extract_content(trg_title, trg_lang)
            if src_content and trg_content:
                print src_content.encode('utf-8') + ' ||| ' + trg_content.encode('utf-8') 

    return

def extract_content(title, lang):
    content = ''
    #title = ''.join(i for i in title if i not in string.punctuation)
    #title = ''.join(i for i in title if not i.isdigit())

    wikipedia.set_lang(lang)
    try:
        page = wikipedia.page(title)
        content = page.content
        content = re.sub("\s\s+" , " ", content)
        content = re.sub("\n+" , " ", content)
        return content
    except:
        return None

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage: python wiki_content.py <title-file> <lang-pair>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
