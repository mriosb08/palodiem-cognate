import os
import dill
import sys
from collections import defaultdict
import re

def tree():
        return defaultdict(tree)

def main(argv):
    """
    Reads zoo-name space (tree of subtitle files) and outputs parallel data with cdec format
    It uses simple heuristics to filter empty subtitles
    The source and target are languages from the moses zoo mapping.
    For example, es - Spanish, pt - Portuguese
    """
    (pickle_file, source, target, tvids) = argv
    zoo_namespace = dill.load(open(pickle_file, "rb"))
    s = dict()
    t = dict()
    en = dict()
    #source = re.compile(source)
    #target = re.compile(target)

    list_vids = [i for i in tvids.split(',')]


    for title in zoo_namespace.iterkeys():
        for video in zoo_namespace[title].iterkeys():
            for lang in zoo_namespace[title][video].iterkeys():
                if zoo_namespace[title][video][lang]['num_lines'] != 0:
                    key = title + '|||' + video
                    if key not in list_vids:
                        if re.match(source, lang, re.IGNORECASE):
                            s[video] = load_file(zoo_namespace[title][video][lang]['path'])
                        if re.match(target, lang, re.IGNORECASE):
                            print target, lang
                            t[video] = load_file(zoo_namespace[title][video][lang]['path'])
                    #if lang.startswith('en'):
                        #print lang
                        #en[video] = load_file(zoo_namespace[title][video][lang]['path'])
    print list_vids
    print len(s.keys())
    print len(t.keys())

    for key in s.iterkeys():
        if key in t:
            if len(s[key]) == len(t[key]):
                for i,j in zip(s[key], t[key]):
                    if i and j:
                        #print e, '\t', i, '\t', j
                        print '%s ||| %s'%(i, j)
    return

def load_file(file_name):
    return [line.strip() for line in open(file_name)]

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:python tr_zoo_v3.py <zoo-pickle> <source> <target> <list-vids>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

