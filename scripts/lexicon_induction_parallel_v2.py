import multiprocessing
import sys
import logging
from more_itertools import chunked
import distance
from collections import defaultdict
import re
import tempfile
import subprocess
import operator

def worker(num, source_chunk, target_list, threshold, window, logger, output, n_best):
    """
    thread worker function
    searchs for a cognate in parallel (number of workers) within a window of frequencies.
    threshold only search for words over a certain frequency.
    """
    logger.info('Worker: %d'%num)
    logger.info('num of words: %d'%len(source_chunk))
    worker_file = output + '.lex-tmp.' + str(num)
    out_tmp = open(worker_file, 'w+')
    
    for (source, source_freq) in source_chunk:
        tmp_scores = dict()
        a = source_freq - window
        b = source_freq + window
        for bin in xrange(a, b):
            if bin in target_list:
                chunk_target = target_list[bin]
                for target in chunk_target:
                    key = target + '|||'+ str(bin)
                    tmp_scores[key] = distance.levenshtein(source, target, normalized=True)
            #pick the min score
        if len(tmp_scores) != 0:
            #ey, score) = min(tmp_scores.iteritems(), key=lambda tmp_scores: tmp_scores[1])  
            sorted_scores = sorted(tmp_scores.items(), key=operator.itemgetter(1))
            #(trg_cognate, trg_freq) = key.split('|||')
            #line = source + '\t' + str(source_freq)  + '\t' + trg_cognate + '\t' + trg_freq + '\t' + str(score) + '\n'
            for rank, tup in enumerate(sorted_scores[:n_best]):
                (key_tup, score) = tup
                (trg_cognate, trg_freq) = key_tup.split('|||')
                line = source + '\t' + str(source_freq)  + '\t' + trg_cognate + '\t' + trg_freq + '\t'+ str(rank) + '\t' + str(score) + '\n'
                out_tmp.write(line)
    logger.info('worker finished')
    return

def load_source(source_file, threshold):
    """
    loads source file 
    cleans numbers and links
    """
    chunk = dict()
    with open(source_file) as sf:
        for line in sf:
            line = line.strip()
            (source_freq, source) = line.split('\t')
            #skip numbers and links
            if re.search(r'\d+', source) or re.search('http', source):
                continue
            if int(source_freq) < threshold:
                continue
            chunk[source.lower()] = (source.lower(), int(source_freq))
    return chunk

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks


if __name__ == '__main__':
    if len(sys.argv) != 8:
        print 'usage:python lexicon_induction_parallel_v2.py <source-file> <target-file> <threshold> <workers> <window> <output> <n-best>'
        sys.exit(1)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    source_file = sys.argv[1]
    target_file = sys.argv[2]
    threshold = int(sys.argv[3])
    num_jobs = int(sys.argv[4])
    window = int(sys.argv[5])
    output = sys.argv[6]
    n_best = int(sys.argv[7])
    #chunks = list(chunked(load_source(source_file), num_jobs))
    source_words = load_source(source_file, threshold)
    logger.info('source side loaded')
    source_chunks = chunks(source_words, num_jobs)
    jobs = []
    target_list = defaultdict(list)
    with open(target_file) as tf:
        for line in tf:
            line = line.strip()
            (target_freq, target) = line.split('\t')
            if re.search(r'\d+', target) or re.search('http', target):
                continue
            if int(target_freq) < threshold:
                continue
            target_list[int(target_freq)].append(target.lower())
    logger.info('target side loaded')
    for i in range(num_jobs):
        p = multiprocessing.Process(target=worker, args=(i, source_chunks[i], target_list, threshold, window, logger, output, n_best))
        p.start()
        jobs.append(p)
    for j in jobs:
        j.join()
        logger.info('%s.exitcode = %s' % (j.name, j.exitcode))
    cmd = 'cat %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    with open(output, 'w') as o:
        for line in p.stdout:
            o.write(line)
    cmd_del = 'rm %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd_del, shell=True, stdout=subprocess.PIPE)
