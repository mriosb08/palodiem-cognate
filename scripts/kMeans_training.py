import sys
from sklearn.cluster import MiniBatchKMeans
import pickle
import numpy as np
import re

def main(args):
    (features_file, n, size, output) = args
    n = int(n)
    size = int(size)
    (words, X) = load_feat(features_file)
    print X.size
    print X
    mbk = MiniBatchKMeans(init='k-means++', n_clusters=n, batch_size=size,
            n_init=10, max_no_improvement=10, verbose=0)
    print 'training...'
    mbk.fit(X)
    print 'dump:', output
    pickle.dump(mbk, open(output, "wb"))
    pickle.dump(words, open('words.'+output, "wb"))
    return

def load_feat(feat_file):
    feat = []
    words = []
    i = 0
    with open(feat_file) as features:
        for line in features:
            line = line.strip()
            cols = re.split('\s+', line)
            words.append(cols[0])
            features = [float(col) for col in cols[1:]]
            feat.append(features)
            i += 1
    return (words, np.asarray(feat))

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:python kMeans_training.py <target-feat> <n-clusters> <batch-size> <output-pickle>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

