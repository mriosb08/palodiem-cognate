import multiprocessing
import sys
import logging
from more_itertools import chunked
import distance
from collections import defaultdict
import re
import tempfile
import subprocess
import operator

def worker(num, source_chunk, target_list, threshold, position, n_char, n_best, logger, output):
    """
    thread worker function
    searchs for a cognate in parallel (number of workers) within a window of frequencies.
    threshold only search for words over a certain frequency.
    """
    logger.info('Worker: %d'%num)
    logger.info('num of words: %d'%len(source_chunk))
    worker_file = output + '.lex-tmp.' + str(num)
    out_tmp = open(worker_file, 'w+')
    
    for (source, source_freq) in source_chunk:
        tmp_scores = dict()
        if position == 'beg':
            try:
                key = source[:n_char]
            except:
                key = source
        else:
            try:
                key = source[-n_char:]
            except:
                key = source
        
        if key.lower() in target_list:
            for target, freq in target_list[key.lower()]:
                key_score = target + '|||'+ str(freq)
                #print source, target
                tmp_scores[key_score] = distance.levenshtein(source.lower(), target.lower(), normalized=True)
            #pick the min score
        if len(tmp_scores) != 0:
            sorted_scores = sorted(tmp_scores.items(), key=operator.itemgetter(1))
            for rank, tup in enumerate(sorted_scores[:n_best]):
                (key_tup, score) = tup
                (trg_cognate, trg_freq) = key_tup.split('|||')
                line = source + '\t' + str(source_freq)  + '\t' + trg_cognate + '\t' + trg_freq + '\t'+ str(rank) + '\t' + str(score) + '\n'
                out_tmp.write(line)       
    logger.info('worker finished')
    return

def load_source(source_file, threshold):
    """
    loads source file 
    cleans numbers and links
    """
    chunk = dict()
    with open(source_file) as sf:
        for line in sf:
            line = line.strip()
            try:
                (source_freq, source) = line.split('\t')
            except:
                (source_freq, source) = line.split()
            #skip numbers and links
            if re.search(r'\d+', source) or re.search('http', source):
                continue
            if int(source_freq) < threshold:
                continue
            chunk[source] = (source, int(source_freq))
    return chunk

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks


if __name__ == '__main__':
    if len(sys.argv) != 9:
        print 'usage:python lexicon_induction_parallel_v3.py <source-file> <target-file> <threshold> <workers> <beg|end> <n-char> <n_best> <output>'
        sys.exit(1)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    source_file = sys.argv[1]
    target_file = sys.argv[2]
    threshold = int(sys.argv[3])
    num_jobs = int(sys.argv[4])
    position = sys.argv[5]
    n_char = int(sys.argv[6])
    n_best = int(sys.argv[7])
    output = sys.argv[8]
    #chunks = list(chunked(load_source(source_file), num_jobs))
    source_words = load_source(source_file, threshold)
    logger.info('source side loaded')
    source_chunks = chunks(source_words, num_jobs)
    jobs = []
    target_list = defaultdict(list)
    with open(target_file) as tf:
        for line in tf:
            line = line.strip()
            try:
                (target_freq, target) = line.split('\t')
            except:
                (target_freq, target) = line.split()
            if int(target_freq) < threshold:
                continue
            if position == 'beg':
                try:
                    key = target[:n_char]
                except:
                    key = target
            else:
                try:
                    key = target[-n_char:]
                except:
                    key = target
            target_list[key.lower()].append((target, target_freq))
    logger.info('target side loaded')
    for i in range(num_jobs):
        p = multiprocessing.Process(target=worker, args=(i, source_chunks[i], target_list, threshold, position, n_char, n_best, logger, output))
        p.start()
        jobs.append(p)
    for j in jobs:
        j.join()
        logger.info('%s.exitcode = %s' % (j.name, j.exitcode))
    cmd = 'cat %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    with open(output, 'w') as o:
        for line in p.stdout:
            o.write(line)
    cmd_del = 'rm %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd_del, shell=True, stdout=subprocess.PIPE)
