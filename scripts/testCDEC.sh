#steps to test a new document with cdec
#input:
#1) document, 2) path to training ini, 2) path to cdec decode ini, 3) path to cdec weights, 4) path to grammar
#1
if [[ $# -ne 6 ]] ; then
    echo 'usage: sh testCDEC.sh <text> <path-training> <path-decode> <path-weights> <path-grammar> <lang-pair>'
    exit 1
fi
echo 'Preprocessing input' $1
#/data/mrios/workspace/sw/cdec-2014-10-12/corpus/tokenize-anything.sh < $1 | /data/mrios/workspace/sw/cdec-2014-10-12/corpus/lowercase.pl 2>> $1.log 1> $1.$6.lc
#2
echo 'Extractring grammar into' $1.$6.sgm
python -m cdec.sa.extract -c $2 -g $5 -j 4 -z < $1 1> $1.$6.sgm 2>> $1.log
#3
echo 'Decoding with grammar' $1.$6.sgm 'with weigth' $3 'into' $1.$6.trans
/data/mrios/workspace/sw/cdec-2014-10-12/decoder/cdec -c $3 -w $4 -i $1.$6.sgm 2>> $1.log 1> $1.$6.trans
