use utf8;
my $threshold = 30;
while(my $line = <>){
    chomp($line);
    $line =~ s/^\s+//;
    next if($line =~ m/[[:punct:]]/g);
    @cols = split(/\s+/, $line);
    next if($cols[0] <= $threshold);
    $cols[1] = lc($cols[1]);
    print "$cols[1]\n";
}
