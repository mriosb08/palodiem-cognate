# -*- coding: utf-8 -*-
import sys
id_col = 1
def main(args):
    (nbest_file, src_file, trg_file) = args
    oov = load_oov(src_file, trg_file)
    with open(nbest_file) as nb:
        for line in nb:
            line = line.strip()
            cols = line.split(' ||| ')
            if len(line.strip()) == 0:
                print line
                continue
            try:
                cols[id_col] = subs_oov(cols[id_col], oov)
                print ' ||| '.join(cols) 
            except:
                print line
    return

def load_oov(src_file, trg_file):
    oov = dict()
    src = [line.strip() for line in open(src_file)]
    trg = [line.strip() for line in open(trg_file)]
    oov = dict(zip(src, trg))
    return oov

def subs_oov(line, oov):
    new_line = []
    for token in line.split(' '):
        if token in oov:
            new_line.append(oov[token])
        else:
            new_line.append(token)
    return ' '.join(new_line)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python cdec_nbestOOV.py <n-best> <src-oov> <trg-oov>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
