# -*- coding: utf-8 -*-
import sys
from space import Space
import numpy as np

def main(args):
    (trg_file, vec_trg) = args
    target_words = set([line.strip() for line in open(trg_file)])
    target_sp = Space.build(vec_trg, target_words)
    for trg in target_words:
        if trg in target_sp.row2id:
            vec_trg = target_sp.mat[target_sp.row2id[trg], :]
            #print np.asarray(vec_trg).ravel()
            output = trg + ' ' + ' '.join(str(x) for x in np.asarray(vec_trg).ravel())
            print output

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python extractFeatTRG.py <trg-words> <trg-vec>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
