import sys
from collections import defaultdict, Counter
def main(args):
    """
    show cognate list coverage 1-gram over test corpus (cdec format, note: use tokenized lowercased file) stats 
    and list for each word possible trasnlation or OOV
    input: 
    test corpus
    cognate list; format:   
    """
    (test_corpus, cognate_file) = args
    words = load_corpus(test_corpus)
    cog = load_cog(cognate_file)
    oov = []
    for word in words:
        if word not in cog:
            oov.append(word)
            print word, '\t<OOV,%s>'%words[word]
        #else:
            #print '\t'.join([str(i) for i in cog[word]])

    print >> sys.stderr, 'STDERR>> number of words in test: ', len(words)
    print >> sys.stderr, 'STDERR>> number of words in cognate list: ', len(cog)
    print >> sys.stderr, 'STDERR>> number of OOV words: ', len(oov) 


    return

def load_cog(cog_file):
    cog = dict()
    with open(cog_file) as cf:
        for line in cf:
            line = line.strip()
            (src, src_freq, trg, trg_freq, score) = line.split('\t')
            cog[src.lower()] = (src.lower(), int(src_freq), trg.lower(), int(trg_freq), float(score))
    return cog

def load_corpus(corpus_file):
    words = Counter()
    with open(corpus_file) as cf:
        for line in cf:
            line = line.strip()
            (src, trg) = line.split(' ||| ')
            for token in src.split():
                words[token] += 1
    return words

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python cognate_coverage.py <test-corpus> <cognate-list>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
