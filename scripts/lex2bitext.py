import sys

def main(args):
    """
    trasnform output of lexicon induction to parallel data in cdec format
    it has the option swap to change the direction of source and target
    """
    if len(args) == 2:
        (lex_file, swap) = args
    else:
        lex_file = args[0]
        swap = None
    sep = ' ||| '
    with open(lex_file) as input:
        for line in input:
            line = line.strip()
            (a, fa, b, fb, score) = line.split('\t')
            if swap:
                print b + sep + a
            else:
                print a + sep + b
    return 


if __name__ == '__main__':
    if len(sys.argv) > 3 or len(sys.argv) < 2:
        print 'usage:python lex2bitext.py <lex-file> [swap]'
        sys.exit(1)
    else:
        main(sys.argv[1:])
