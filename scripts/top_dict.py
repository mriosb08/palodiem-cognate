import sys
from collections import defaultdict

def main(args):
    top = defaultdict(list)
    with open(args[0]) as in_f:
        for line in in_f:
            line = line.strip()
            (src, trg, score) = line.split(' ')
            top[src].append((trg, float(score)))

        for src,trg in top.iteritems():
            sorted_trg = sorted(trg, key=lambda tup: tup[1])
            (t, s) = sorted_trg[-1]
            print '%s %s'%(src, t)
    return
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'usage:python top_dict.py <dict-lex>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
