import sys
import numpy as np
import string

def main(in_file):
    """
    statistics from alinged data in cdec format
    number of words in source and target sides
    note: use tokenized file from cdec pipeline
    """
    ratios = []
    table = string.maketrans("","")
    with open(in_file) as inf:
        for line in inf:
            line = line.strip()
            (src, trg) = line.split(' ||| ')
            src= src.translate(table, string.punctuation)
            trg = trg.translate(table, string.punctuation)
            len_src = len(src.split())
            len_trg = len(trg.split())
            ratios.append(abs(len_src - len_trg))

        print 'average', np.average(ratios)
        print 'mean', np.mean(ratios)
        print 'std', np.std(ratios)
        print 'var', np.var(ratios)
        print 'histo', np.histogram(ratios)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'usage:python parallel_len_ratios.py <parallel-file>'
        sys.exit(1)
    else:
        main(sys.argv[1:])

