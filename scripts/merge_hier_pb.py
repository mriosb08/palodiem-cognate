# -*- coding: utf-8 -*-
import sys
from collections import defaultdict
import re

def main(args):
    (hier_file, pb_file) = args
    hier_trans = load_hiero(hier_file)
    pb_trans = load_pb(pb_file)

    for id in sorted(hier_trans.keys()):
        print '%s ||| %s'%(id, pb_trans[id])
        for tran in hier_trans[id]:
            if re.search(r"\s+", tran):
                print '%s ||| %s'%(id, tran)
    return

def load_hiero(hier_file):
    hier_trans = defaultdict(list)
    with open(hier_file) as hrf:
        for line in hrf:
            line = line.strip()
            try:
                (id, trans) = line.split(' ||| ')
                hier_trans[int(id)].append(trans)
            except:
                (id, trans) = line.split(' |||')
                hier_trans[int(id)].append(trans)
    return hier_trans

def load_pb(pb_file):
    pb_trans = [line.strip() for line in open(pb_file)]
    return pb_trans

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage:python merge_hier_pb.py <cdec_n-best> <moses_n-best>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
