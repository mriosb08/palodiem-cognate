#!/usr/bin/perl
#use utf8;
binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");
#input tokenized and lowercased file
while(<STDIN>){
    my $line = $_;
    chomp($line);
    $line =~ s/[[:punct:]]//g;
    $line =~ s/\d+//g;
    $line =~ s/\s+/ /g;
    next if $line =~ /^\s*$/;
    next if $line =~ /^http/;
    my @new_line = ();
    my @tokens = split(/\s+/, $line);
    for my $token(@tokens){
        my @temp = $token =~ /(.{1,2})/g;
        push(@new_line, join(' ', @temp));
        #push(@new_line, '#');  #TODO uncomment for sentences!!! 
    }
    #foreach my $char(@chars) {
    #    if ($char =~ m/\s/){
    #        push(@new_line, '@s@');
    #    }else{
    #        push(@new_line, $char);
    #    }
    #}
    print join(' ', @new_line), "\n";
}
