#!/usr/bin/perl

while (<>) {
    my $line = $_;
    chomp($line);
    $line =~ s/^\s+//;
    my @tokens = split(/\s+/, $line);
    print "$tokens[0]\t$tokens[1]\n";
}
