import importlib
import sys

def main(args):
    """
    given a filtering model quit pairs of words that produce noise from a lex file
    cognate words that do not meet a requierement
    """
    (lex_file, model_name, config_file) = args
    
    loaded_class = load_class(model_name)
    model = loaded_class()

    if hasattr(model, 'configure'):
        model.configure(config_file)
    if hasattr(model, 'initialize'):
        model.initialize()

    with open(lex_file) as input:
        for line in input:
            line = line.strip()
            (a, fa, b, fb, score) = line.split('\t')
            if model.process_entry(a, int(fa), b, int(fb), float(score)):
                print '\t'.join([a, fa, b, fb, score])
    return

def load_class(full_class_string):
    """
    dynamically load a class from a string
    the user can develop new models in filter_models.py by extending the base class
    for example the class ThresholdScore
    """
    class_data = full_class_string.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]
    module = importlib.import_module(module_path)
    # Finally, we retrieve the Class
    return getattr(module, class_str)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage: python filter_lex.py <lex-file> <model-name> <config-file>'
        #lex-file output from lexicon-induction
        #example model name and config file:
        #model-name: filter_models.ThesholdScore
        #config-file: threshold_score.ini 
        sys.exit(1)
    else:
        main(sys.argv[1:])
