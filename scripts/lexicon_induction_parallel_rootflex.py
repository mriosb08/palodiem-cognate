import multiprocessing
import sys
import logging
from more_itertools import chunked
import distance
from collections import defaultdict
from nltk.stem.snowball import SnowballStemmer
import re
import subprocess
import operator

def worker(num, source_chunk, target_list, threshold, window, logger, w1, w2, output, n_best=10):
    """
    thread worker function
    searchs for a cognate in parallel (number of workers) within a window of frequencies.
    threshold only search for words over a certain frequency.
    the score is weigthed over the root and flexion (w1 and w2).
    the user can give more importance to measure the roots or flexions of words.
    """
    logger.info('Worker: %d'%num)
    logger.info('num of words: %d'%len(source_chunk))
    worker_file = output + '.lex-tmp.' + str(num)
    out_tmp = open(worker_file, 'w+')

    for (source, source_freq) in source_chunk:
        tmp_scores = dict()
        (src, root_src, flex_src) = source
        a = source_freq - window
        b = source_freq + window
        for bin in xrange(a, b):
            if bin in target_list:
                chunk_target = target_list[bin]
                for (trg, root_trg, flex_trg) in chunk_target:
                    root_dist = distance.levenshtein(root_src, root_trg, normalized=True)
                    flex_dist = distance.levenshtein(flex_src, flex_trg, normalized=True)
                    score_combo = w1*root_dist + w2*flex_dist
                    key = trg + '|||' + str(bin)
                    tmp_scores[key] = score_combo
            #pick the min score
        if len(tmp_scores) != 0:
            sorted_scores = sorted(tmp_scores.items(), key=operator.itemgetter(1))
            for rank, tup in enumerate(sorted_scores[:n_best]):
                (key_tup, score) = tup
                (trg_cognate, trg_freq) = key_tup.split('|||')
                line = src + '\t' + str(source_freq)  + '\t' + trg_cognate + '\t' + trg_freq + '\t'+ str(rank) + '\t' + str(score) + '\n'
                out_tmp.write(line)
    logger.info('worker finished')
    return

def load_source(source_file, threshold, stemmer):
    """
    loads source file 
    cleans numbers and links
    """
    chunk = dict()
    with open(source_file) as sf:
        for line in sf:
            line = line.strip()
            (source_freq, source) = line.split('\t')
            if re.search(r'\d+', source) or re.search('http', source):
                continue
            if int(source_freq) < threshold:
                continue
            try:
                root = stemmer.stem(source.decode('utf-8'))
                flex = source.lower().replace(root, '')
            except:
                root = source.lower()
                flex = ''
            chunk[root] = ((source,root, flex), int(source_freq))
    return chunk

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks


if __name__ == '__main__':
    if len(sys.argv) != 9:
        print 'usage:python lexicon_induction_parallel_rootflex.py <source-file> <target-file> <lang-pair> <weights-w1_w2> <threshold> <workers> <window> <output>'
        #NOTE install NLTK and NLTK data to use stemmer!!!
        #pip install -U nltk (on virtualenv)
        sys.exit(1)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    source_file = sys.argv[1]
    target_file = sys.argv[2]
    (src, trg) = sys.argv[3].split('-') #lang pair is format es-pt
    (w1, w2) = sys.argv[4].split('-')   #w ranges from 0 to 1, both wiegths must sum 1 For example, 0.8-0.2,  w1=0.8 and w2=0.2 the root is more important
    threshold = int(sys.argv[5])
    num_jobs = int(sys.argv[6])
    window = int(sys.argv[7])
    output = sys.argv[8]
    #chunks = list(chunked(load_source(source_file), num_jobs))
    src_stemmer = SnowballStemmer(src) #it uses NLTK snowball for spliting the root and flexion
    trg_stemmer = SnowballStemmer(trg)
    source_words = load_source(source_file, threshold, src_stemmer)
    source_chunks = chunks(source_words, num_jobs)
    jobs = []
    target_list = defaultdict(list)
    with open(target_file) as tf:
        for line in tf:
            line = line.strip()
            (target_freq, target) = line.split('\t')
            if int(target_freq) < threshold:
                continue
            try:
                root = trg_stemmer.stem(target.decode('utf-8'))
                flex = target.lower().replace(root, '')
            except:
                root = target.lower()
                flex = ''
            target_list[int(target_freq)].append((target, root, flex))
    logger.info('target side loaded')

    for i in range(num_jobs):
        p = multiprocessing.Process(target=worker, 
                args=(i, source_chunks[i], 
                    target_list, threshold, window, logger, float(w1), float(w2), output))
        jobs.append(p)
        p.start()

    for j in jobs:
        j.join()
        print '%s.exitcode = %s' % (j.name, j.exitcode)
    cmd = 'cat %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    with open(output, 'w') as o:
        for line in p.stdout:
            o.write(line)
    cmd_del = 'rm %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd_del, shell=True, stdout=subprocess.PIPE)
