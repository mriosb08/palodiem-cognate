import sys
from collections import defaultdict, OrderedDict
import string
import re

def main(args):
    (n_file, n) = args
    n_best = defaultdict(list)
    n = int(n)
    with open(n_file) as nf:
        for line in nf:
            line = line.strip()
            cols = line.split('\t')
            try:
                id = int(cols[0])
                trg = cols[1]
                n_best[id].append(trg)
            except:
                id = int(cols[0])
                n_best[id].append('')

    keylist = n_best.keys()
    keylist.sort()
    for key in keylist:
        #best_l = sorted(n_best[key], key=lambda tup: tup[1])[:n]
        #best_l = set([x for x,y in best_l])
        #tup_trg = [(el,0) for el in n_best[key]]
        best_list = sortAndUniq(n_best[key])
        for trg in best_list[:n]:
            print '%s\t%s'%(key, trg)

        
    return


def sortAndUniq(input):
    output = []
    for x in input:
        if x not in output:
            output.append(x)
    #output.sort()
    return output

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'python n-bestRank.py <n-best> <new-n>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
