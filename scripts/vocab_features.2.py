import sys
from collections import defaultdict
from operator import itemgetter
import distance as distl
import pickle
from scipy.spatial import distance
import numpy as np

model_src = None
model_trg = None

def main(args):
    (vocab_file, vec_src, vec_trg, threshold) = args
    global model_src
    global model_trg
    model_src = pickle.load(open(vec_src, "rb"))
    model_trg = pickle.load(open(vec_trg, "rb"))
    with open(vocab_file) as vf:
        src_words = defaultdict(list)
        for line in vf:
            line = line.strip()
            (source, target, score) = line.split()
            #ed1 = distance.levenshtein(source, target, normalized=True)
            #(ed2, ed3, ed4) = wagnerfischer.levenshtein_ids(source, target)
            (src_root, src_flex) = split_token(source)
            (trg_root, trg_flex) = split_token(target)
            cos = cos_context(source, target)
            #print src_root, src_flex
            ed5 = distl.levenshtein(src_root, trg_root, normalized=True)
            ed6 = distl.levenshtein(src_flex, trg_flex, normalized=True)
            ed = [1.0-ed5, 1.0-ed6, cos]
            src_words[source].append((target, ed, score))

        for src, trg_list in src_words.iteritems():
            #is prob  of alignment high means high rank
            trg_sorted = sorted(trg_list, key=itemgetter(2), reverse=True)
            rank = 0
            for tup in trg_sorted:
                (trg, features, score) = tup
                ed1 = features[0]
                if ed1 < float(threshold):
                    continue
                print "%s\t%s\t%s\t%s"%(src, trg, rank , '\t'.join(map(str, features)))
                rank += 1
    return

def split_token(token):
    l = len(token) / 2
    try:
        root = token[:l]
        flex = token[l:]
        return (root, flex)
    except:
        return (token, token)

def cos_context(a, b):
    global model_src
    global model_trg
    cos_vec = 0.0
    x = []
    z = []
    key_src = a.lower() 
    if  key_src in model_src:
        x = model_src[key_src] #source vector
    key_trg = b.lower()
    if key_trg in model_trg:
        z = model_trg[key_trg] #target vector
    if len(x) != 0 and len(z) != 0 :
        cos_vec = distance.cosine(np.asarray(x), np.asarray(z))

    return cos_vec

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:python vocab_features.2.py <vocab-file> <vec-src> <vec-trg> <threshold>'
        sys.exit(1)
    else:
        main(sys.argv[1:])




