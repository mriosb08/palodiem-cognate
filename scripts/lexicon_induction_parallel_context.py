# -*- coding: utf-8 -*-
import multiprocessing
import sys
import logging
from more_itertools import chunked
import distance as distl
from collections import defaultdict
import re
import tempfile
import subprocess
from gensim import corpora, models, similarities
from scipy.spatial import distance
import numpy as np
import pickle
import operator

model_src = None #source binary vectors from word2vec
model_trg = None #target binary vectors from word2vec
w = None         #trained w matrix for mapping from source space to target space

def main(argv):
    """
    searchs for cogantes in wikipedia by maximazing similar spelling and similar context
    spelling is 1 - edit_distance(a,b)
    context are vectors from word embeddings
    """
    global model_src
    global model_trg
    global w

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    source_file = argv[0]
    target_file = argv[1]
    threshold = int(argv[2])
    num_jobs = int(argv[3])
    window = int(argv[4])
    output = argv[5]
    vec_src = argv[6]
    vec_trg = argv[7]
    w_file = argv[8]
    w1 = argv[9]
    w2 = argv[10]
    n_best = argv[11]
    #chunks = list(chunked(load_source(source_file), num_jobs))
    source_words = load_source(source_file, threshold)
    logger.info('source side loaded')
    source_chunks = chunks(source_words, num_jobs)
    jobs = []
    target_list = defaultdict(list)
    
    #model_src = models.word2vec.Word2Vec.load_word2vec_format(vec_src, binary=True)
    #model_trg = models.word2vec.Word2Vec.load_word2vec_format(vec_trg, binary=True)
    model_src = pickle.load(open(vec_src, "rb"))
    model_trg = pickle.load(open(vec_trg, "rb"))
    w = pickle.load(open(w_file, "rb")) #w is a mapping matrix so the comparion of vectors is done in the same space
    w = w[0]
    logger.info('vectors loaded')

    with open(target_file) as tf:
        for line in tf:
            line = line.strip()
            (target_freq, target) = line.split('\t')
            if int(target_freq) < threshold:
                continue
            target_list[int(target_freq)].append(target)
    logger.info('target side loaded')
    for i in range(num_jobs):
        p = multiprocessing.Process(target=worker, args=(i, source_chunks[i], target_list, threshold, window, logger, output, w1, w2, n_best))
        p.start()
        jobs.append(p)
    for j in jobs:
        j.join()
        logger.info('%s.exitcode = %s' % (j.name, j.exitcode))
    cmd = 'cat %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    with open(output, 'w') as o:
        for line in p.stdout:
            o.write(line)
    cmd_del = 'rm %s.lex-tmp.*' % output
    p = subprocess.Popen(cmd_del, shell=True, stdout=subprocess.PIPE)
    return


def worker(num, source_chunk, target_list, threshold, window, logger, output, w1, w2, n_best):
    """
    thread worker function
    searchs for cognates withing a window of frequencies
    it maximizes two features spelling and word embeddings
    """
    logger.info('Worker: %d'%num)
    logger.info('num of words: %d'%len(source_chunk))
    worker_file = output + '.lex-tmp.' + str(num)
    out_tmp = open(worker_file, 'w+')
    global model_src #source side vectors
    global model_trg #target side vectors
    global w         #matrix for mapping from source space into target space
    w1 = float(w1)
    w2 = float(w2)
    n_best = int(n_best)
    for (source, source_freq) in source_chunk:
        tmp_scores = dict()
        key_src = source.lower() + ':' + str(source_freq)
        if  key_src in model_src:
            x = model_src[key_src] #source vector
        else:
            #continue
            x = []
        a = source_freq - window
        b = source_freq + window
        for bin in xrange(a, b):
            if bin in target_list:
                chunk_target = target_list[bin]
                for target in chunk_target:
                    key_trg = target.lower() + ':'+ str(bin)
                    key_trg2 = target.lower() + '|||'+ str(bin)
                    if key_trg in model_trg:
                        z = model_trg[key_trg] #target vector
                    else:
                        z = []
                    #print x
                    #print z
                    if len(z) != 0 and len(x) !=0:
                        #x_prime = x * w #mapping into z space TODO uncomment
                        cos_vec = distance.cosine(np.asarray(x), np.asarray(z)) # cosine between vectors
                    else:
                        cos_vec = 0.0
                    sim = 1 - distl.levenshtein(source, target, normalized=True) #edit distance modified to be similarity
                    #print key_src, key_trg, sim, cos_vec

                    tmp_scores[key_trg2] = (w1 * sim + w2 * cos_vec) / (w1 + w2) #final score 
            #pick the max score
        if len(tmp_scores) != 0:
            sorted_scores = sorted(tmp_scores.items(), key=operator.itemgetter(1), reverse=True)
            for rank, tup in enumerate(sorted_scores[:n_best]):
                (key_tup, score) = tup
                (trg_cognate, trg_freq) = key_tup.split('|||')
                line = source + '\t' + str(source_freq)  + '\t' + trg_cognate + '\t' + trg_freq + '\t'+ str(rank) + '\t' + str(score) + '\n'
                out_tmp.write(line)
    logger.info('worker finished')
    return

def load_source(source_file, threshold):
    """
    loads source side 
    skips numbers, links and words under a certain frequency
    """
    chunk = dict()
    with open(source_file) as sf:
        for line in sf:
            line = line.strip()
            (source_freq, source) = line.split('\t')
            #skip numbers and links
            if re.search(r'\d+', source) or re.search('http', source):
                continue
            if int(source_freq) < threshold:
                continue
            chunk[source] = (source, int(source_freq))
    return chunk

def chunks(l, amount):
    """
    splits the lists of words in chunks for each worker
    """
    if amount < 1:
        raise ValueError('amount must be positive integer')
    l = l.values()
    chunks = []
    chunk_len = len(l) // amount
    leap_parts = len(l) % amount
    remainder = amount // 2  # make it symmetrical
    i = 0
    while i < len(l):
        remainder += leap_parts
        end_index = i + chunk_len
        if remainder >= amount:
            remainder -= amount
            end_index += 1
        chunks.append(l[i:end_index])
        i = end_index
    return chunks


if __name__ == '__main__':
    if len(sys.argv) != 13:
        print 'usage:python lexicon_induction_parallel_context.py <source-file> <target-file> <threshold> <workers> <window> <output> <vec-scr> <vec-trg> <vec-W> <w1> <w2> <n-best>'
        sys.exit(1)
    main(sys.argv[1:])
