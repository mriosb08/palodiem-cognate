# -*- coding: utf-8 -*-
import sys
from collections import defaultdict
import gzip
def main(args):
    (dict_file, order, i) = args
    pb = defaultdict(list)
    i = int(i)
    if dict_file.endswith('gz'):
        input_file = gzip.open(dict_file)
    else:
        input_file = open(dict_file)
    for line in input_file:
        line = line.strip()
        cols = line.split(' ||| ')
        src = cols[0]
        trg = cols[1]
        scores = cols[2].split(' ')
        score = scores[i]
        try:
            pb[src].append((trg, float(score)))
        except:
            print >>sys.stderr, 'wrong phrase table format', line

    for src, trgs in pb.iteritems():
        if order == 'reverse':
            trgs = sorted(trgs, key=lambda tup: tup[1], reverse=True)
        else:
            trgs = sorted(trgs, key=lambda tup: tup[1])
        for (trg, score) in trgs:
            print '%s\t%s\t%s'%(src, trg, str(score))
    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python max_pt.py <pb-dict> <order> <pos>'
        print """POS is one of the following:
        0 inverse phrase translation probability φ(f|e)
        1 inverse lexical weighting lex(f|e)
        2 direct phrase translation probability φ(e|f)
        3 direct lexical weighting lex(e|f)
        """
        sys.exit(1)
    else:
        main(sys.argv[1:])
