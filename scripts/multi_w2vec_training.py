import sys
import pickle
import numpy as np
from scipy.optimize import minimize
from scipy.optimize import anneal
from gensim import corpora, models, similarities
from numpy import linalg as LA

x = None
z = None

def trans_matrix(w):
    """
    function to optimize w mapping matrix
    """
    global x
    global z
    res = sum([(LA.norm(x_i * (w - z_i)))**2  for x_i,z_i in zip(x, z)]) 
    return res


def main(args):
    """
    trains a matrix w to map from source space to target space
    """
    (a_file, src_file, trg_file, size_w, output_pickle) = args
    global x
    global z
    x = []
    z = []
    #w = np.zeros(int(size_w))
    w = np.random.rand(int(size_w)) #number of dimansions used to train word2vec
    #note train word2vec as binary files
    #For example,
    #./word2vec -train textX -output vectorsx.bin -cbow 1 -size 200 -window 8 -negative 25 -hs 0 -sample 1e-4 -threads 20 -binary 1 -iter 15
    src_model = models.word2vec.Word2Vec.load_word2vec_format(src_file, binary=True)
    trg_model = models.word2vec.Word2Vec.load_word2vec_format(trg_file, binary=True)
    with open(a_file) as af:
        for line in af:
            line = line.strip()
            (src, trg, score_1, score_2) = line.split() #a file is the moses ouput vocab file
            if src.lower() in src_model and trg.lower() in trg_model:
                x.append(src_model[src.lower()])
                z.append(trg_model[trg.lower()])

    x = np.array(x)
    z = np.array(z)
    print 'training shape: ', x.shape
    print 'initial w: ', w
    #res = minimize(trans_matrix, w, method='BFGS', options={'disp': True})
    #minimize w with similated annealing with the function trans_matrix
    res = anneal(trans_matrix, w, schedule='boltzmann', full_output=True
            , maxiter=100, lower=-1.0, upper=1.0, dwell=50, disp=True)
    print res
    pickle.dump(res, open(output_pickle, "wb")) #store w into a pickle
    return

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print 'usage:python  multi_w2vec_training.py <alignment-file> <source-vecbin> <target-vecbin> <size-w> <output-pickle>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
