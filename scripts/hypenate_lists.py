import sys
import pyphen
import codecs
import re

def main(argv):
    (lang_pair, separator, lex_file) = argv
    (source_lang, target_lang) = lang_pair.split('-')
    source_dict = pyphen.Pyphen(lang=source_lang)
    target_dict = pyphen.Pyphen(lang=target_lang)
    with codecs.open(lex_file, 'r', 'utf-8') as lf:
        for line in lf:
            line = line.strip()
            values = line.split('\t')
            source = values[0]
            target = values[1]
            source_hypen = source_dict.inserted(source, hyphen=separator)
            target_hypen = target_dict.inserted(target, hyphen=separator)
            source_hypen = source_hypen.replace(' ','')
            target_hypen = target_hypen.replace(' ','')
            source_hypen += separator
            target_hypen += separator
            #print source_hypen,'\t', target_hypen, '\t', '\t'.join(values[2:])
            #source_hypen = encode_utf8_to_iso88591(source_hypen.lower())
            #target_hypen = encode_utf8_to_iso88591(target_hypen.lower())
            print '%s\t%s'%(source_hypen.encode('utf-8').lower(), target_hypen.encode('utf-8').lower())
    return

def encode_utf8_to_iso88591(utf8_text):
    """
    Encode and return the given UTF-8 text as ISO-8859-1 (latin1) with
    unsupported characters replaced by '?', except for common special
    characters like smart quotes and symbols that we handle as well as we
    can.
    For example, the copyright symbol => '(c)' etc.

    If the given value is not a string it is returned unchanged.

    References:

        en.wikipedia.org/wiki/Quotation_mark_glyphs#Quotation_marks_in_Unicode
        en.wikipedia.org/wiki/Copyright_symbol
        en.wikipedia.org/wiki/Registered_trademark_symbol
        en.wikipedia.org/wiki/Sound_recording_copyright_symbol
        en.wikipedia.org/wiki/Service_mark_symbol
        en.wikipedia.org/wiki/Trademark_symbol
    """
    if not isinstance(utf8_text, basestring):
        return utf8_text
    # Replace "smart" and other single-quote like things
    utf8_text = re.sub(
    u'[\u02bc\u2018\u2019\u201a\u201b\u2039\u203a\u300c\u300d]', "'", utf8_text)
    # Replace "smart" and other double-quote like things
    utf8_text = re.sub(u'[\u00ab\u00bb\u201c\u201d\u201e\u201f\u300e\u300f]', '"', utf8_text)
    # Replace copyright symbol
    utf8_text = re.sub(u'[\u00a9\u24b8\u24d2]', '(c)', utf8_text)
    # Replace registered trademark symbol
    utf8_text = re.sub(u'[\u00ae\u24c7]', '(r)', utf8_text)
    # Replace sound recording copyright symbol
    utf8_text = re.sub(u'[\u2117\u24c5\u24df]', '(p)', utf8_text)
    # Replace service mark symbol
    utf8_text = re.sub(u'[\u2120]', '(sm)', utf8_text)
    # Replace trademark symbol
    utf8_text = re.sub(u'[\u2122]', '(tm)', utf8_text)
    # Replace/clobber any remaining UTF-8 characters that aren't in ISO-8859-1
    return utf8_text.encode('ISO-8859-1', 'replace')

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage: python hypenate_lists.py <lang-pair> <separator> <filtered-file>'
        print 'note: after run convert output (utf-8) into Latin1 (iso-8859-1)'
        sys.exit(1)
    else:
        main(sys.argv[1:])
