import sys
import argparse 
from collections import defaultdict
import itertools
def main():
    parser = argparse.ArgumentParser(description='python rank2svm.py')
    parser.add_argument('-t', action="append", dest='tasks',
            help='Training datasets', required=True)
    parser.add_argument('-o', action="store", dest='output',
            help='prediction output')
    parser.add_argument('-m', action="store", dest='max',
            help='Max number of ranks', required=True)
    parser.add_argument('-p', action="store", dest='test_id',
            help='number of test task', default=-1)
    parser.add_argument('-n', action="store", dest='num_tasks',
            help='number of tasks', default=-1)
    
    results = parser.parse_args()
    tasks = results.tasks
    max = int(results.max)
    X = load_file(tasks, max)
    test_id = int(results.test_id)
    num_tasks = int(results.num_tasks)
    feature_augmentation(X, test_id, num_tasks)
     

    return

def feature_augmentation(X, test_id, num_tasks):
    #output = []
    if num_tasks == -1:
        num_tasks = len(X)
    flag_task = 0
    for id_task, task_line in enumerate(X):
        if test_id != -1:
            id_task = test_id
        for line in task_line:
            (quid, tup) = line
            (src, trg, rank, features) = tup
            aug_features = []
            if test_id == None or test_id == -1:
                aug_features.append(features) #general!!!
            else:
                aug_features.append([0.0] * len(features))
            #copy = ['%s:%s'%(str(j+1), str(0.0)) for j in range(len(features))]
            for i in range(num_tasks):
                #different to current task
                if id_task != i: 
                    copy = [0.0] * len(features)
                    aug_features.append(copy)
                #current task
                else:
                    aug_features.append(features)

            merged = list(itertools.chain.from_iterable(aug_features))
            new_features = ['%s:%s'%(i+1, str(merge_feature)) for i, merge_feature in enumerate(merged)]            
            print '%s %s %s #%s,%s,%s'%(rank, quid, ' '.join(new_features), src, trg, rank)
            #output.append('%s %s %s #%s,%s,%s'%(rank, quid, ' '.join(new_features), src, trg, id_task))
    return 



def load_file(feature_files, max):
    """
    load feature file with format: src target rank featue1 feature2 .. featureN
    """
    X = []
    quid = 1
    for ff in feature_files:
        with open(ff) as f:
            pairs = defaultdict(list)
            for line in f:
                line = line.strip()
                #print line
                #p = re.compile(r'\t')
                cols = line.split('\t')
                #pairs.append((cols[0], cols[1]))
                src = cols[0]
                trg = cols[1]
                rank = int(cols[2]) +1 
                features = cols[3:]
                pairs[src].append((src, trg, rank, features))
            task_line = []
            #ranking 
            for source  in pairs.keys():
                trg_list = pairs[source]
                for tup in trg_list[:max]: 
                    task_line.append(('qid:%s'%(str(quid)), tup))
                quid += 1
            X.append(task_line)
    #print len(X) 
    #print y
    #print len(y)
    return (X)

if __name__ == '__main__':
    main()
