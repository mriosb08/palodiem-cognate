import distance
import sys

def main(argv):
    """
    filters pairs of words from moses vocab files given a score threshold
    there are 3 different metrics:
    [levenshtein, hamming, jaccard]

    """
    (distance_name, threshold, lex_file) = argv
    output = dict()
    with open(lex_file) as lf:
        for line in lf:
            line = line.strip()
            (source, target, score) = line.split()
            edit_distance = 1.0
            if distance_name == 'levenshtein':
                edit_distance = distance.levenshtein(source, target, normalized=True)
                #print source, target, result
            elif distance_name == 'hamming':
                edit_distance = distance.hamming(source, target)
            elif distance_name == 'jaccard':
                edit_distance = distance.jaccard(source, target)
            else:
                edit_distance = distance.sorensen(source, target)
            if edit_distance <= float(threshold):
                #print source, '\t', target, '\t', edit_distance, '\t', score 
                print source, ' ||| ', target

    return

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage: python distance_filter.py <distance> <threshold> <lex-file-moses>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
