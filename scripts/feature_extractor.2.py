# -*- coding: utf-8 -*-
import sys
mport distance as distl
import pickle
from scipy.spatial import distance
import numpy as np

model_src = None
model_trg = None


def main(args):
    (lex_file, c_src, c_trg) = args
    #print args
    global model_src
    global model_trg

    model_src = pickle.load(open(c_src, 'rb')) 
    model_trg = pickle.load(open(c_trg, 'rb'))
    #print 'caches loaded..'
    with open(lex_file) as lf:
        for line in lf:
            line =  line.strip()
            (source, f_src, target, f_trg, rank, score) = line.split('\t')
            cos = cos_context(source, target)
            (src_root, src_flex) = split_token(source)
            (trg_root, trg_flex) = split_token(target)
            ed5 = distl.levenshtein(src_root, trg_root, normalized=True)
            ed6 = distl.levenshtein(src_flex, trg_flex, normalized=True)
            features = [1.0-ed5, 1.0-ed6, cos]
            print '%s\t%s\t%s\t%s'%(source, target, rank, '\t'.join(map(str, features)))
    return

def context(token, freq, cache_context):
    key = token + ':' + freq
    if key in cache_context:
        return cache_context[key]
    else:
        return ('','')

def split_token(token):
    l = len(token) / 2
    try:
        root = token[:l]
        flex = token[l:]
        return (root, flex)
    except:
        return (token, token)

def cos_context(a, b):
    global model_src
    global model_trg
    cos_vec = 0.0
    x = []
    z = []
    key_src = a.lower() 
    if  key_src in model_src:
        x = model_src[key_src] #source vector
    key_trg = b.lower()
    if key_trg in model_trg:
        z = model_trg[key_trg] #target vector
    if len(x) != 0 and len(z) != 0 :
        cos_vec = distance.cosine(np.asarray(x), np.asarray(z))
    return cos_vec

def prepro_token(token, num_stem):
    if len(token) == num_stem:
        return (token, token)
    try:
        root = token[:num_stem]
        flex = token[num_stem:]
        return (root, flex)
    except:
        return (token, token)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python feature_extractor.2.py <lex-file> <cache-src> <cache-trg>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
