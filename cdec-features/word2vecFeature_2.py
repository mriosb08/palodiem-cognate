import cdec.sa
from gensim import corpora, models, similarities
import scipy as sp

model = None

@cdec.sa.configure
def configure(config):
    """
    loads the wrd2vec binary vector
    note: modify cdec ini to point for word2vec_file
    """
    global model
    model = models.word2vec.Word2Vec.load_word2vec_format(config['word2vec_file'], binary=True)

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def word2vec_feature_1(ctx):
    """
    compares monolingual data f sentence with test sentence
    the score is the cosine between vectors
    """
    testSent = cdec.sa.decode_sentence(ctx.test_sentence)
    fsentEdit = [] #list of the lengths of the training sentences observed for fphrase
    for m in ctx.matches:
        sentId = ctx.f_text.get_sentence_id(m[0]) 
        fsent = ctx.f_text.get_sentence(sentId)
        sim = []
        for ftoken in fsent[:-1]:
            if str(ftoken) in model:
                fvec = model[str(ftoken)]
            for test_token in testSent[1:-1]:
                if str(test_token) in model:
                    tvec = model[str(test_token)]
                    sim.append(sp.spatial.distance.cosine(fvec, tvec))
                else:
                    sim.append(0.0)
        fsentEdit.append(sum(sim)/float(len(sim)))    
    return sum(fsentEdit)/float(len(fsentEdit)) 

