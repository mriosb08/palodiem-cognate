import cdec.sa
import distance
import pyphen

source_dict = None
target_dict = None


@cdec.sa.configure
def configure(config):
    """
    load dictionares to hypenate the source and target sides
    """
    global source_dict
    global target_dict
    source_dict = pyphen.Pyphen(lang=config['src_lang'])
    target_dict = pyphen.Pyphen(lang=config['trg_lang'])

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def edit_distance_2(ctx):
    """
    compares the test sentence with the e sentence where the unit is a shyllable instead of chars
    returns the edit distance average of the matches sentences from training and the test sentence
    """
    #f = ctx.f_text
    testSent = cdec.sa.decode_sentence(ctx.test_sentence)
    fsentEdit = [] #list of the lengths of the training sentences observed for fphrase
    separator = ' '
    global source_dict
    global target_dict
    for m in ctx.matches:
        sentId = ctx.f_text.get_sentence_id(m[0]) 
        fsent = ctx.f_text.get_sentence(sentId)
        esent = ctx.e_text.get_sentence(sentId)
        edit_distance = []
        for etoken in esent[:-1]:
            source_hypen = source_dict.inserted(etoken.decode('utf-8'), hyphen=separator)
            elist = source_hypen.split(' ')
            for ftoken in testSent[1:-1]:
                target_hypen = target_dict.inserted(ftoken.decode('utf-8'), hyphen=separator)
                flist = target_hypen.split(' ')
                edit_distance.append(distance.levenshtein(elist, flist, normalized=True))
        fsentEdit.append(sum(edit_distance)/float(len(edit_distance)))    
    return sum(fsentEdit)/float(len(fsentEdit))

