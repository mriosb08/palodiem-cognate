import cdec.sa
import distance
import re
import itertools
#import numpy as np

@cdec.sa.configure
def configure(config):
    pass

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def edit_distance_3(ctx):
    """
    compares the terminals of the e and f rules without the non-terminals
    retutrns the average similairty of the edit distance between words
    """
    ephrase = [s.split() for s in filter(None,re.split("\[X,[12]\]", str(ctx.ephrase)))]
    fphrase = [s.split() for s in filter(None,re.split("\[X,[12]\]", str(ctx.fphrase)))]
    ephrase =  list(itertools.chain(*ephrase))
    fphrase =  list(itertools.chain(*fphrase))
    edit_distance = []
    for e in ephrase:
        for f in fphrase:
            edit_distance.append(distance.levenshtein(e, f, normalized=True))
    result = 1 - (sum(edit_distance)/float(len(edit_distance)))
    return result 

