import cdec.sa
from gensim import corpora, models, similarities

#import numpy as np

model = None

@cdec.sa.configure
def configure(config):
    """
    loads word2vec vectors
    note: modify cdec ini file to point for the binary vectors file
    word2vec_file = 'w2vec.source.bin'
    """
    global model
    model = models.word2vec.Word2Vec.load_word2vec_format(config['word2vec_file'], binary=True)

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def word2vec_feature_1(ctx):
    """
    compares monolingual test sentence with f sentence
    the score is cosine between the vectors of words
    return the average of edit distance between the matches f sentences and the test sentence
    """
    testSent = cdec.sa.decode_sentence(ctx.test_sentence)
    testSentLen = len(testSent)-2 #minus 2 because of the "<s>" and "</s>" in the testSent-representation
    fsentEdit = [] #list of the lengths of the training sentences observed for fphrase
    for m in ctx.matches:
        sentId = ctx.f_text.get_sentence_id(m[0]) 
        fsent = ctx.f_text.get_sentence(sentId)
        sim = []
        for ftoken in fsent[:-1]:
            for test_token in testSent[1:-1]:
                sim.append(model.similarity(str(ftoken), str(test_token)))
        fsentEdit.append(sum(sim)/float(len(sim)))    
    return sum(fsentEdit)/float(len(fsentEdit)) 

