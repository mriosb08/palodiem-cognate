import cdec.sa
import cPickle as pickle

sim_cache = None

@cdec.sa.configure
def configure(config):
    """
    loads a cache of similariy scores where the key is a pair of words a ||| b and the value is their similarity score
    note: modify cdec ini with the name of the cache pickle file
    cache_file = 'cache.pickle'
    """
    global sim_cache
    sim_cache = pickle.load(open(config['cache_file'], 'rb'))

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def indicator_feature_1(ctx):
    """
    compares the test setence with the e sentence via a cache if the words are present in e and test the 
    feature returns 1
    """
    testSent = cdec.sa.decode_sentence(ctx.test_sentence)
    indicator = 0
    global sim_cache
    for m in ctx.matches:
        sentId = ctx.f_text.get_sentence_id(m[0]) 
        esent = ctx.e_text.get_sentence(sentId)
        for etoken in esent[:-1]:
            for ftoken in testSent[1:-1]:
                key = str(etoken) + '|||' + str(ftoken)
                if key in sim_cache:
                    indicator = 1
    return indicator

