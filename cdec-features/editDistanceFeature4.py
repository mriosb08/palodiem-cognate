import cdec.sa
import distance
from nltk.stem.snowball import SnowballStemmer

src_stemmer = None
trg_stemmer = None


@cdec.sa.configure
def configure(config):
    """
    loads the dictionaries to stem the source and target side
    note: chanche ini of cdec to point the lang used. For example,  es 
    """
    global src_stemmer
    global trg_stemmer
    src_stemmer = SnowballStemmer(config['src_stemmer'])
    trg_stemmer = SnowballStemmer(config['trg_stemmer'])

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def edit_distance_4(ctx):
    """
    compares the test sentence with the e sentence where the unit is the root of the word
    returns the average of the edit distance between the matched sentences from training and the test sentence
    """
    testSent = cdec.sa.decode_sentence(ctx.test_sentence)
    fsentEdit = [] #list of the lengths of the training sentences observed for fphrase
    global src_stemmer
    global trg_stemmer
    for m in ctx.matches:
        sentId = ctx.f_text.get_sentence_id(m[0]) 
        esent = ctx.e_text.get_sentence(sentId)
        edit_distance = []
        #print fsent 
        for etoken in esent[:-1]:
            source_stem = src_stemmer.stem(etoken.decode('utf-8'))
            for ftoken in testSent[1:-1]:
                target_stem = trg_stemmer.stem(ftoken.decode('utf-8'))
                edit_distance.append(distance.levenshtein(source_stem, target_stem, normalized=True))
        fsentEdit.append(sum(edit_distance)/float(len(edit_distance)))    
    return sum(fsentEdit)/float(len(fsentEdit))

