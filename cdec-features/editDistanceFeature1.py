import cdec.sa
import distance

@cdec.sa.configure
def configure(config):
    pass

@cdec.sa.annotator
def my_annotation(words):
    return None

@cdec.sa.feature
def edit_distance_1(ctx):
    """
    feature that comprares the edit distance between wrds from the test sentence with the
    e sentence
    returns the similarity average between the matched sentences in traingin and each test sentence 
    """
    testSent = cdec.sa.decode_sentence(ctx.test_sentence)
    testSentLen = len(testSent)-2 #minus 2 because of the "<s>" and "</s>" in the testSent-representation
    fsentEdit = [] #list of the lengths of the training sentences observed for fphrase
    for m in ctx.matches:
        sentId = ctx.f_text.get_sentence_id(m[0]) 
        fsent = ctx.f_text.get_sentence(sentId)
        esent = ctx.e_text.get_sentence(sentId)
        edit_distance = []
        #for etoken in fsent[:-1]:
        #    for ftoken in testSent[1:-1]:
        #        edit_distance.append(distance.levenshtein(etoken, ftoken, normalized=True))
        #fsentEdit.append(sum(edit_distance)/float(len(edit_distance)))  
        fsentEdit.append(distance.levenshtein(esent[:-1], testSent[1:-1], normalized=True))
    result = 1 - sum(fsentEdit)/float(len(fsentEdit))
    return result 

