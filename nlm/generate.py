from random import random
import re

class generate:
    def __init__(self, lm, order=4, usemax=False, num_stop=2, end='$'):
        """
        Generates characters given a LM
        param lm: LM dictionary
        param order: order of the LM (default 7 chars)
        param usemax: if true return next max liklely character (default false)
        param num_stop: generate chacarter up to N times the stop symbol is find (default 2)
        param en: end of sentence (default $)
        """
        self.lm = lm
        self.order = order
        self.usemax = usemax
        self.num_stop = num_stop
        self.end = end
        return
    def generate_letter(self, history):
        """
        given input characters generate next letter
        """
        history = history[-self.order:]
        if history in self.lm:
            dist = self.lm[history] #query the LM with input character
            if self.usemax: #if usemax return more likely next character
                return max(dist,key=lambda item:item[1])[0]
            else: #return ordered list of following characters
                dist_sorted = sorted(dist, key=lambda item:item[1], reverse=True)
                return [i for i,_ in dist_sorted]
        else:
            return ''
    def generate_text(self, history, nletters=4):
        """
        generate up to n characters
        param hystory: input characters
        param nletters: num of characters to generate
        """
        out = [history]
        for i in xrange(nletters):
            c = self.generate_letter(history)
            history = history[-self.order:] + c
            out.append(c)
        return "".join(out)

    def generate_text_stop(self, history, stop_sym=' '):
        """
        generate characters up to N times stop symbol is find
        param hystory: input characters
        param stop_sym: symbol to stop (default space)
        """
        out = [history]
        n = 0
        if stop_sym == self.end: #if stop is end of sentence stop at one
            self.num_stop = 1
        while True:
            c = self.generate_letter(history) #generate next character queaty the LM
            if n > self.num_stop or c == '': #if reaches n stop times break
                break
            if c == stop_sym:
                n += 1
            history = history[-self.order:] + c #generate new story to query the LM
            out.append(c)
        return "".join(out)

    def generate_options(self, history, stop_sym=' ', options=5):
        """
        TODO not tested!!!!
        """
        out = [history]
        n = 0
        self.usemax = False
        prev_space = 0
        opts = [None] * options
        if stop_sym == self.end:
            self.num_stop = 1
        for i in enumerate(options):
            while True:
                c = self.generate_letter(history)
                if c[0] == stop_sym:
                    n += 1
                    prev_space += 1
                elif prev_space == 1:
                    prev_space += 1
                if n >= self.num_stop or c[0] == '':
                    break
                if len(c) >= i and prev_space > 1:
                    history = history[-self.order:] + c[i]
                    prev_sapce = 0
                    out.append(c[i])
                else:
                    history = history[-self.order:] + c[0]
                    out.append(c[0])
            opts.append(out)
        return opts

                
        


