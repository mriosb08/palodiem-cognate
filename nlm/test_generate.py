from lm import lm
from generate import generate
import sys
#import pickle
import cPickle as pickle

def main(args):
    (src_file, trg_pickle, n, s) = args
    n = int(n)
    print 'loading LM...'
    trg_lm = pickle.load(open(trg_pickle, "rb")) #load LM
    #to known the order of the lm pick one entry and count chars
    #n = len(trg_lm[0])
    with open(src_file) as srcf: #load source sentences
        for line in srcf:
            line = line.strip()
            (id, src) = line.split('\t')
            id = int(id) #id of sentence
            try:
                (trg_id, lm) = trg_lm[id] #each entry is a tuple (id, LM)
                                        #, where id is position in src, LM is target
                                        #id to retirver target LM
                gen = generate(lm=lm, order=n, usemax=True, num_stop=2) #generate characters with order n, 
                                                                        #most probable characters,
                                                                        #stop after 2 spaces
                try:
                    while True: #while input 7 characters ctrl-c to continue nest sentence
                        print 'src[%s]: '%(id), src
                        history = raw_input("Text input: ") #user input 7 characters
                        if len(history) <= n: #if 7 characters generate characters 
                            #l = len(line) - n
                            output = gen.generate_text_stop(history, stop_sym=s)
                            print 'OUTPUT: ', output #predicted characters
                except KeyboardInterrupt:
                    print 'interrupted!'
            except:
                print 'No LM available for id %s'%(str(id))


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:python test_generate.py <src-file> <trg-pickle> <n-order> <stop-sym>'
        """
            Input: 
                param src-file: with format position <tab> segment
                param trg-pickle: file with LM for each trasnlation 
                param n-order: order of the LM (it can should be the same as in training)
                param stop-sym:  ' ' space or $ (end of sentence)
                generate can produce up to n letters given the stop symbol default 2.
        """
        sys.exit(1)
    else:
        main(sys.argv[1:])

