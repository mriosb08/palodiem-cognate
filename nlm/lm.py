from collections import *
import re

class lm:
    def __init__(self, fname=None, segments=[], order=4, join_sym=' $$$ '):
        self.fname = fname
        self.segments = segments
        self.order = order
        self.join_sym = join_sym 
        return

    def train_char_lm(self):
        if self.fname:
            data = file(self.fname).read()
        else:
            data = self.join_sym.join(self.segments)

        data = re.sub(r'\n', ' ', data)
        data = re.sub(r'\s+', ' ', data)
        lm = defaultdict(Counter)
        pad = "~" * self.order
        data = pad + data
        for i in xrange(len(data)-self.order):
            history, char = data[i:i+self.order], data[i+self.order]
            lm[history][char]+=1
        def normalize(counter):
            s = float(sum(counter.values()))
            return [(c,cnt/s) for c,cnt in counter.iteritems()]
        outlm = {hist:normalize(chars) for hist, chars in lm.iteritems()}
        return outlm


