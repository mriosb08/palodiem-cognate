from lm import lm
from generate import generate
import sys
#import pickle
import time
import cPickle as pickle
from collections import defaultdict

def main(args):
    (input_file, n, output_file) = args
    per_segment_lm = []
    n = int(n)
    n_best = load_nbest(input_file)
    print 'training'
    for key in sorted(n_best.iterkeys()):
        print 'id: ', key, ' num segments: ', len(n_best[key])
        lm_sh = lm(segments=n_best[key], order=n)
        t0 = time.time()
        char_lm = lm_sh.train_char_lm()
        t1 = time.time()
        print '\ttime training: ', (t1-t0)
        per_segment_lm.append((key, char_lm))
    print 'dump pickle'
    pickle.dump(per_segment_lm, open(output_file, "wb"))


def load_nbest(input_file):
    n_best = defaultdict(list)
    with open(input_file) as inf:
        for line in inf:
            line = line.strip()
            cols = line.split(' ||| ')
            if len(cols) >= 2:
                n_best[int(cols[0])].append(cols[1])
            else:
                n_best[int(cols[0])].append('</None>')
    return n_best


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'usage:python train_lm.py <n-best> <n> <output-pickle>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
