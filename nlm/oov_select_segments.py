import sys

def main(args):
    (src_file, oov_file, max_oov, max_sent) = args
    max_oov = int(max_oov)
    max_sent = int(max_sent)
    src = [line.strip() for line in open(src_file)]
    oov = [line.strip() for line in open(oov_file)]
    i = 0
    for s, o in zip(src, oov):
        try:
            cols = o.split()
            num_oov = len(cols)
        except:
            num_oov = 0
        if i > max_sent:
            break
        if s != '' and (num_oov < max_oov):
            print '%s\t%s'%(i, s)

        i += 1
    return

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage:pythn oov_select_segments.py <src> <oov-list> <max-OOV> <max-segments>'
        sys.exit(1)
    else:
        main(sys.argv[1:])
