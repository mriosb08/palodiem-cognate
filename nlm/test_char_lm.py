from lm import lm
from generate import generate
import sys


if __name__ == '__main__':
    fname = sys.argv[1]
    n = int(sys.argv[2])
    l = int(sys.argv[3])
    lm_sh = lm(fname, n)
    print 'TRAINING: ', fname
    char_lm = lm_sh.train_char_lm()
    gen = generate(char_lm, n)
    try:
        while True:
            history = input("Text input: ")
            if len(history) <= n:
                output = gen.generate_text(history, nletters=l) 
                print 'OUTPUT: ', output                        
    except KeyboardInterrupt:
        print 'interrupted!'
