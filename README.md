# Palodiem project repository for SMT based on cognate information #
**University of Leeds**

## Repo description ##
* required-tools.txt: tools requiered to run scripts and pipelines
* scripts: scripts used to extract cognates from Wikipedia, process Zoo data and Open subtitles 
* cdec-pipeline: scripts to run the whole translation process (pipeline) hierharchical SMT
* cdec-features: feature functions for cdec
* moses-pipeline: scripts to run the whole translation process (pipeline) phrase-based SMT
* moses-features: feature functions for moses
* commands.txt: examples of how to run some scripts (one on each directory)